package com.exadel.i.webshop.web.controllers;

import com.exadel.i.webshop.dao.CategoryDAO;
import com.exadel.i.webshop.dao.OrderDAO;
import com.exadel.i.webshop.domain.Order;
import com.exadel.i.webshop.domain.User;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author Ksenia Orlenko
 */
@Controller
@RequestMapping("/profile")
public class UserProfileController {
    @Resource
    OrderDAO orderDAO;
    @Resource
    CategoryDAO categoryDAO;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String show(Model uiModel, HttpSession session) {
        User user = (User) session.getAttribute("CURRENT_USER");
        uiModel.addAttribute("user", user);
        List<Order> orders = orderDAO.findByUserId(user.getId());
        if (!orders.isEmpty()) {
            uiModel.addAttribute("orders", orders);
        }
        uiModel.addAttribute("categories", categoryDAO.findAll());
        return "profile-show";
    }


}
