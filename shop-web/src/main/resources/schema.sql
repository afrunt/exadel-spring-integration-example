CREATE FUNCTION md5( VARCHAR(128), VARCHAR(10))
RETURNS VARCHAR(256)
LANGUAGE JAVA
DETERMINISTIC
NO SQL
EXTERNAL NAME 'CLASSPATH:org.hsqldb.lib.MD5.encode';

CREATE SEQUENCE ORDER_SEQUENCE
    START WITH 1
    INCREMENT BY 1;

CREATE TABLE Users (
  id           BIGINT GENERATED BY DEFAULT AS IDENTITY,
  firstName    VARCHAR(256) NOT NULL,
  lastName     VARCHAR(256) NOT NULL,
  email        VARCHAR(256) NOT NULL,
  passwordHash VARCHAR(256) NOT NULL,
  phoneNumber  VARCHAR(256) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE Roles (
  id   BIGINT GENERATED BY DEFAULT AS IDENTITY,
  name VARCHAR(256) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE UserRoles (
  userId BIGINT NOT NULL FOREIGN KEY REFERENCES Users (id),
  roleId BIGINT NOT NULL FOREIGN KEY REFERENCES Roles (id),
  PRIMARY KEY (userId, roleId)
);


CREATE TABLE BillingAddresses (
  id      BIGINT GENERATED BY DEFAULT AS IDENTITY,
  userId  BIGINT       NOT NULL FOREIGN KEY REFERENCES Users (id),
  name    VARCHAR(256) NOT NULL,
  state   VARCHAR(256) NOT NULL,
  city    VARCHAR(256) NOT NULL,
  address VARCHAR(256) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE ShippingAddresses (
  id      BIGINT GENERATED BY DEFAULT AS IDENTITY,
  userId  BIGINT       NOT NULL FOREIGN KEY REFERENCES Users (id),
  name    VARCHAR(256) NOT NULL,
  state   VARCHAR(256) NOT NULL,
  city    VARCHAR(256) NOT NULL,
  address VARCHAR(256) NOT NULL,
  zip     VARCHAR(256) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE Categories (
  id   BIGINT GENERATED BY DEFAULT AS IDENTITY,
  name VARCHAR(256) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE Products (
  id             BIGINT GENERATED BY DEFAULT AS IDENTITY,
  name           VARCHAR(256) NOT NULL,
  description    CLOB         NOT NULL,
  imageUrl       VARCHAR(256) NOT NULL,
  countAvailable BIGINT       NOT NULL,
  price          DOUBLE       NOT NULL,
  categoryId     BIGINT       NOT NULL FOREIGN KEY REFERENCES Categories (id),
  PRIMARY KEY (id)
);


CREATE TABLE Orders (
  id                BIGINT       NOT NULL,
  orderDate         DATETIME     NOT NULL,
  orderStatus       SMALLINT     NOT NULL,
  billingAddressId  BIGINT       NOT NULL,
  shippingAddressId BIGINT       NOT NULL,
  total             DOUBLE       NOT NULL,
  phoneNumber       VARCHAR(256) NOT NULL,
  userId            BIGINT       NOT NULL FOREIGN KEY REFERENCES Users (id),
  PRIMARY KEY (id)
);

CREATE TABLE OrderLines (
  id        BIGINT GENERATED BY DEFAULT AS IDENTITY,
  orderId   BIGINT NOT NULL FOREIGN KEY REFERENCES Orders (id) ON DELETE CASCADE,
  productId BIGINT NOT NULL FOREIGN KEY REFERENCES Products (id),
  quantity  BIGINT NOT NULL,
  PRIMARY KEY (id)
);