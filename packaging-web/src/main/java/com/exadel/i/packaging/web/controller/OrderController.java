package com.exadel.i.packaging.web.controller;

import com.exadel.i.packaging.domain.Order;
import com.exadel.i.packaging.service.OrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Andrey Frunt
 */
@Controller
@RequestMapping("/orders")
public class OrderController {
    @Resource
    private OrderService orderService;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String list(Model uiModel) {
        List<Order> allOrders = orderService.findAll();
        if (!allOrders.isEmpty()) {
            return show(allOrders.get(0).getId(), uiModel);
        } else {
            uiModel.addAttribute("orders", allOrders);
            return "list-all";
        }
    }

    @RequestMapping(value = {"/{id}", "/{id}/"}, method = RequestMethod.GET)
    public String show(@PathVariable Long id, Model uiModel) {
        uiModel.addAttribute("orders", orderService.findAll());
        uiModel.addAttribute("currentOrder", orderService.findById(id));
        return "show-order";
    }

    @RequestMapping(value = {"packaging", "/packaging/"}, method = RequestMethod.POST)
    public String packaging(@RequestParam(value = "packagingOrderId") Long id, Model uiModel) {
        orderService.packagingOrder(id);
        uiModel.addAttribute("orders", orderService.findAll());
        return "redirect:/orders/";
    }

    @ResponseBody
    @RequestMapping(value = "/rest/find-all", method = RequestMethod.GET, produces = "application/json")
    public List<Order> findAll() {
        return orderService.findAll();
    }
}
