package com.exadel.i.webshop.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Andrey Frunt
 */
public class ShoppingCart {
    private List<CartItem> items = new ArrayList<CartItem>();

    private double total;
    private int totalProductCount;

    public void clear() {
        items.clear();
        total = 0;
        totalProductCount = 0;
    }

    public List<CartItem> getItems() {
        return Collections.unmodifiableList(items);
    }

    public List<Long> getProductIds() {
        List<Long> ids = new ArrayList<Long>(getItems().size());
        for (CartItem item : items) {
            ids.add(item.getProductId());
        }
        return ids;
    }

    public boolean isEmpty() {
        return items.isEmpty();
    }

    public boolean containsProduct(long productId) {
        for (CartItem item : items) {
            if (item.getProductId() == productId) {
                return true;
            }
        }
        return false;
    }

    public void addItem(CartItem item) {
        items.add(item);
    }

    public void removeItem(CartItem item) {
        items.remove(item);
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getTotalProductCount() {
        return totalProductCount;
    }

    public void setTotalProductCount(int totalProductCount) {
        this.totalProductCount = totalProductCount;
    }
}
