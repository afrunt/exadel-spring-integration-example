package com.exadel.i.revise.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Andrey Frunt
 */
@Controller
@RequestMapping("/")
public class LoginController {
    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String index() {
        return "redirect:/orders/";
    }

    @RequestMapping(value = {"/login", "/login/"}, method = RequestMethod.GET)
    public String showLoginPage(Model uiModel) {
        uiModel.addAttribute("title", "Login page");
        return "login";
    }
}
