INSERT INTO Roles (id, name) VALUES (1, 'USER');
INSERT INTO Roles (id, name) VALUES (2, 'ADMIN');

INSERT INTO Users (id, firstName, lastName, email, passwordHash, phoneNumber)
  VALUES (1, 'John', 'Doe', 'john.doe@gmail.com', md5('test', 'UTF-8'), '01234567899');


INSERT INTO Users (id, firstName, lastName, email, passwordHash, phoneNumber)
  VALUES (2, 'Alex', 'Doe', 'alex.doe@gmail.com', md5('test', 'UTF-8'), '12345678910');

INSERT INTO Users (id, firstName, lastName, email, passwordHash, phoneNumber)
  VALUES (3, 'Jess', 'Doe', 'jess.doe@gmail.com', md5('test', 'UTF-8'), '23456789102');

INSERT INTO UserRoles (userId, roleId) VALUES (1, 1);

INSERT INTO ShippingAddresses (id, userId, name, state, city, address, zip)
  VALUES (1, 1, 'John Doe', 'CA', 'Walnut Creek', '1340 Treat Blvd.', '94597');
INSERT INTO BillingAddresses (id, userId, name, state, city, address)
  VALUES (1, 1, 'John Doe', 'CA', 'Walnut Creek', '1340 Treat Blvd.');

INSERT INTO Categories (id, name) VALUES (1, 'Laptops');
INSERT INTO Categories (id, name) VALUES (2, 'Tablets');
INSERT INTO Categories (id, name) VALUES (3, 'Phones');

INSERT INTO Products (id, name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    1,
    'Fujitsu Lifebook AH512',
    'The Fujitsu Lifebook AH512 is a solid everyday notebook with a high-definition 15.6 inch widescreen display. Additionally you can enjoy all your media on a TV via HDMI. Integrated WLAN and Bluetooth provide you with great connectivity and the spill-resistant keyboard with number pad provides extra reliability and usability.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/fecd389225fa81a5851faf84f87eb66a38876cc2/images/fujitsu_lifebook_ah512_vfy_ah512mpao5ru_7692928.jpg',
    50,
    559,
    1
  );

INSERT INTO Products (id, name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    2,
    'HP ProBook 4540s (B6N37EA)',
    'Your Business Partner. Optimized for Windows® 7, these notebooks are ideal for SMBs. They offer multimedia tools, easy-to-use security and a sleek, vertical brushed aluminum casing. Options include discrete graphics and battery-saving mode, and two display sizes.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/fecd389225fa81a5851faf84f87eb66a38876cc2/images/hp_probook_4540s_b6n37ea_6784406.jpg',
    5,
    670,
    1
  );
INSERT INTO Products (id, name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    3,
    'Lenovo IdeaPad Z575A',
    'Your Business Partner. Optimized for Windows® 7, these notebooks are ideal for SMBs. They offer multimedia tools, easy-to-use security and a sleek, vertical brushed aluminum casing. Options include discrete graphics and battery-saving mode, and two display sizes.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/fecd389225fa81a5851faf84f87eb66a38876cc2/images/copy_lenovo_59-311928_4fa79197449ff_6440116.jpg',
    0,
    459,
    1
  );
INSERT INTO Products (id, name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    4,
    'LenovoX IdeaPad Z575A',
    'Your Business Partner. Optimized for Windows® 7, these notebooks are ideal for SMBs. They offer multimedia tools, easy-to-use security and a sleek, vertical brushed aluminum casing. Options include discrete graphics and battery-saving mode, and two display sizes.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/fecd389225fa81a5851faf84f87eb66a38876cc2/images/copy_lenovo_59-311928_4fa79197449ff_6440116.jpg',
    0,
    459,
    1
  );


INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Apple iPhone 4 8GB White',
    'Product Information The Apple iPhone 4 - 8 GB Smartphone is a cutting-edge smartphone for users who demand excellence in mobile phone technology. The Apple iPhone 4 Smartphone measures 115.2 mm (height) x 58.6 mm (width) x 9.3 mm (depth) and weighs 137 grams, 5GB Cam and etc. ',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/494b9a730d2d46677c2d1ec5dc3c72ce72b81dcc/images/phones/apple_iphone_4_8gb_white_5761896.jpg',
    1,
    269,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Apple iPhone 5 16GB',
    'At 7.6 mm and 112 grams,3 iPhone 5 has a remarkably thin and light design.  And the beveled edges are diamond cut for incredible precision.CPU performance and graphics are up to twice as fast as on the A5 chip. But even with all that speed, iPhone 5 gives you outstanding battery life.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/apple_iphone_5_16gb_black_slate_7110711.jpg',
    1,
    700,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Gigabyte gSmart GS1362',
    'The Gigabyte smartphone is an Android-powered device that offers a variety of features and a large screen that is perfect. The Gigabyte GSmart G1362 offers a candy-bar form factor and comes in black. The smartphone is unlocked and ready to be used any mobile wireless network.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/gigabyte_gsmart_g1362_7316250.jpg',
    1,
    398,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Gigabyte gSmart GS202',
    'The Gigabyte smartphone is an Android-powered device that offers a variety of features and a large screen that is perfect. The Gigabyte GSmart GS202 offers a candy-bar form factor and comes in black. The smartphone is unlocked and ready to be used any mobile wireless network',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/gigabyte_gsmart_gs202_7188108.jpg',
    1,
    499,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'HTC Sensation XL 16GB',
    'HTC Sensation XL is a snazzy-looking touchscreen phone that houses the dual core Snapdragon 1.5 GHz processor for the smoothest mobile multi-tasking experience ever. On the large touchscreen of this HTC smartphone, you can view clear, highly detailed, great quality visuals. ',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/htc_sensation_xl_5471940.jpg',
    1,
    484,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'HTC WinPhone 16GB 8 Black',
    'Simple yet smart, the HTC Windows Phone 8X is amazingly sculpted with a seamless form. Powered by the 1.5 GHz Qualcomm S4, dual-core processor, this HTC cell phone delivers a quick and efficient performance. Moreover, the HTC Windows Phone 8X runs on the Windows Phone 8 OS, which keeps you entertained with a wide range of applications on its intuitive and user-friendly interface. With a wide 4.3-inch touchscreen made of Corning Gorilla Glass 2, this HTC cell phone is resistant to damage and everyday wear and tear. Thanks to an 8 MP camera, this black smartphone allows you to capture all the wonderful moments of your life with great clarity. Furthermore, the 16 GB memory of this HTC cell phone lets you comfortably store a lot of your media content. Also, the Wi-Fi 802.11 a/b/g/n connectivity in this smartphone ensures that you stay connected to the Web even while you are on-the-go.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/htc_win_phone_8x_black_50ab60e4a4b8d_7506786.jpg',
    1,
    693,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'HTC WinPhone 8x Black',
    'Simple yet smart, the HTC Windows Phone 8X is amazingly sculpted with a seamless form. Powered by the 1.5 GHz Qualcomm S4, dual-core processor, this HTC cell phone delivers a quick and efficient performance. Moreover, the HTC Windows Phone 8X runs on the Windows Phone 8 OS, which keeps you entertained with a wide range of applications on its intuitive and user-friendly interface. With a wide 4.3-inch touchscreen made of Corning Gorilla Glass 2, this HTC cell phone is resistant to damage and everyday wear and tear. Thanks to an 8 MP camera, this black smartphone allows you to capture all the wonderful moments of your life with great clarity. Furthermore, the 16 GB memory of this HTC cell phone lets you comfortably store a lot of your media content. Also, the Wi-Fi 802.11 a/b/g/n connectivity in this smartphone ensures that you stay connected to the Web even while you are on-the-go.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/htc_win_phone_8x_black_7377180.jpg',
    1,
    300,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'LG Optimus 4x P880',
    'Enjoy various Google applications on the LG Optimus 4X HD Android phone that runs on the Ice Cream Sandwich operating system. Powered by a 1.5 GHz quad-core Tegra 3 processor, this LG smartphone delivers excellent performance. Boasting a 4.7-inch IPS HD screen, the LG Optimus 4X HD smartphone displays clear text and lifelike images. Capture your memorable moments in stunning clarity with the 8 MP camera of this Android phone. Moreover, with an internal memory of 16 GB, this LG smartphone allows you to store all your favorite videos, movies, etc.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/lg_optimus_4x_6160487.jpg',
    1,
    458,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'LG Optimus Black P970',
    'With a Gorilla Glass display, the LG Optimus P970 mobile is extremely durable and can put up with rugged usage. The 5 megapixel camera of this LG cellular phone lets you click superior quality pictures. Featuring support for the geo-tagging functionality, this LG mobile allows you to add identification to your photos, videos etc.. This LG cellular phone is 3G capable, and provides easy and fast access to the internet. The 2 GB internal memory of the LG Optimus P970 mobile provides enough room for all types of programs, while 32 GB (maximum) expandable memory allows you to store music, videos, and much more. This LG mobile is A-GPS ready, thus providing faster and more accurate navigation.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/lg_optimus_black_p970_white_5072730.jpg',
    1,
    420,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'LG Optimus L3 E400 1GB White',
    'The LG Optimus L3, A New Style of Android Phone that is both Simple and Powerful – The LG Optimus L3 is a member of LG’s new range of L-style handsets which aims to offer users desirable and stylish mobiles. LG plans to release a new product line in early 2012. Begining with the Optimus L3 which offers the experience of an advanced smartphone in a slim body. LG Optimus L3 comes with a rectangular panel that contrast with metallic accents.The New LG Optimus L3 has a 3.2-inch capacitive display, which keeps it''s users conveniently and continuously connected to social media and social networks, with a large capacity battery to power everything. The LG Optimus L3 runs Android 2.3.5 Gingerbread OS and powered by an 800MHz Dual-Core Processor, 2GB RAM as well as 150MB internal memory.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/lg_optimus_l3_white_6323907.jpg',
    1,
    359,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'LG Optimus L7 P700 4GB Black',
    'Enjoy various Google Mobile services like Gmail, YouTube, and Gtalk with the LG Optimus L7 P700 that runs on Android 4.0 Ice Cream Sandwich OS. A 4.3-inch touchscreen of this LG smartphone brings various functions at your fingertips. Capture those moments on the spur with the 5 MP camera of this LG Optimus L7 P700. Thanks to the Wi-Fi connectivity in this Android smartphone, you can surf the Internet, check e-mails, and stream videos at amazing speeds. GPS support in this LG smartphone provides navigational assistance to chart the best route. For music enthusiasts, the LG Optimus L7 P700 Android smartphone comes with a music player that keeps you entertained with all your favorite tracks and movie songs.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/lg_optimus_l7_4fbdf0b158ba4_6546388.jpg',
    1,
    459,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'LG Optimus VU 4GB',
    'Browse the web at high speed with the LG Optimus Vu smartphone that supports 4G connectivity. Enjoy various Google applications on this LG smartphone that runs on Android v4.0 operating system. Powered by a 1.5 GHZ dual core processor, this Android phone delivers excellent performance. Capture your memorable moments in stunning clarity with the 8 MP camera of the LG Optimus Vu Android phone. Moreover, the 5-inch HD-IPS LCD capacitive touchscreen of this LG smartphone displays clear text and lifelike images.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/lg_optimus_vu_7241928.jpg',
    1,
    500,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Nokia 500 2GB Azure Blue',
    'The Nokia cell phone offers features such as 3G network generation, GSM network technology, GPS tracking capability, and a 5.0 MP digital camera. The 3.2-inch TFT touchscreen on the Nokia 500 unlocked smartphone allows the user to control the phone and select applications with just a touch of the finger. With the integrated Bluetooth technology, the user can also connect this Nokia cell phone to a Bluetooth headset, a computer, Scala rider, several car models, or any other device with Bluetooth-enabled technology. This versatile Nokia 500 unlocked smartphone comes with an Internet browser to surf the Web, check email, and perform a variety of Web-based functions. Moreover, the Nokia cell phone features an integrated application that enables the user to check Web-based or non-Web-based email accounts. Featuring a speakerphone and 2 GB memory capacity, this azure blue device is also Wi-Fi-capable. This compact phone measures 4.4 inches high, 2.1 inches wide, 0.6 inches deep, and it weighs 3.28 oz.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/nokia_500_azure_blue_4e3911bc835fa_5112217.jpg',
    1,
    199,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Nokia Lumia 800 16GB White',
    'Stylish and sensational, the Nokia Lumia 800 is a high-end smartphone designed to operate in GSM and CDMA networks. This Nokia smartphone runs on Windows Phone 7.5 (Mango) mobile platform that is preloaded with applications like Nokia Drive, TopApps, Nokia Music, and more. This Windows mobile features a 3.7-inch ClearBlack AMOLED display that is ideal for viewing photos, watching movies, and browsing the web. The Nokia Lumia 800 has an 8 MP camera with a Carl Zeiss lens that lets you capture photographs with stunning quality. The camera also supports HD video recording at 30 fps. As this Nokia smartphone comes with 3G and Wi-Fi support, you can access the internet at blazing speeds. The pre-loaded XBOX Live Hub application on this Windows mobile lets you play XBOX games on the phone. The Nokia Lumia 800 features an on-board GPS with A-GPS support, offering navigational assistance to reach your destination.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/nokia_800_white_6194471.jpg',
    1,
    409,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Nokia Asha 200 Black',
    'Powerful and handy, the Nokia Asha 200 is a 2 MP smartphone designed to operate with dual SIM cards. This Nokia phone has a 2.4-inch LCD display that delivers clear and bright images. As this 2 MP smartphone comes with a WAP browser, you can browse and download content from the Internet. Using the 2 MP camera of the Nokia Asha 200, you can capture your cherished memories. The microSD card slot of this Nokia phone enables you to expand the memory capacity by up to 32 GB, so that you can store your favorite music and videos. The battery of the Nokia Asha 200 allows you to talk for 7 hours, hours in a single charge.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/nokia_asha_200_black_4ea93ca3941ec_5541557.jpg',
    1,
    199,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Nokia Lumia 510 Black',
    'The device is going to run Windows Phone 7.5 on a single-core Snapdragon S1 processor with 256MB of RAM. It will have 4GB of built-in storage. To make up for its lack of a memory card slot, it will come with 7GB of free SkyDrive storage.The Lumia 510 will include a 4-inch 800 x 480 pixel (WVGA) TFT touchscreen, a 5MP rear-facing camera, and a 1300 mAh battery.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/nokia_lumia_510_black_50a9eb0381c3a_7496345.jpg',
    1,
    299,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Nokia Lumia 820 1GB Blue',
    'Among the native features of this black Nokia Lumian 820 are an integrated media player and a web browser, but the features available on the smartphone do not stop there. This Nokia cell phone comes with 3.5 mm audio jack for privately enjoying music. The smartphone also supports Bluetooth 3.1 in addition to possessing integrated Wi-Fi system. A microSD card slot allows users of this Nokia cell phone to expand the 8 GB of storage by up to 32 GB with a compatible microSD card. The space created on the smartphone by such a card can also be used along with its 8 MP camera to obtain and store photos or videos. The Nokia Lumian 820 possesses a multi-touch touchscreen that allows for users to navigate the phone''s various. In addition, several types of instant messaging prog can be used with this black Nokia cell phone. This phone is unlocked, which means it can be used with any cell phone service provider.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/nokia_lumia_820_blue_5051c77f3b3cf_7101158.jpg',
    1,
    529,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Nokia Lumia 900 White',
    'Powered by a 1.4 GHz processor, the Nokia Lumia 900 cell phone makes multitasking a breeze. As it runs on the Windows Phone 7.5 Mango OS, this Nokia Lumia phone lets you access a wide range of applications. Navigating through this Windows smartphone is a breeze, thanks to its large, 4.3-inch ClearBlack AMOLED touchscreen display. Stream your favorite movies and music at blazing speeds, as the Nokia Lumia 900 cell phone supports 4G. You can surf the Internet even while youâ€™re on the go, with Wi-Fi connectivity on this Windows smartphone. Capture memorable moments of your life in amazing quality using the 8 MP camera of this Nokia Lumia phone.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/nokia_lumia_900_white_6879439.jpg',
    1,
    549,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Samsung GalaxyS3 I9300 32GB PebbleBlue',
    'Designed for humans and inspired by nature, the Samsung Galaxy S III GT-I9300 cell phone sees, listens, and responds to your requirements. A front-facing camera on this Samsung Galaxy S phone works with the Smart Stay feature to adjust the brightness for a continued viewing pleasure. Sharing content with other S III users is as simple as putting the backs of two Samsung Galaxy S III GT-I9300 cell phones together. Enjoy multitasking with the Pop up Play technology of this 32 GB Samsung Galaxy S smartphone that lets you chat with friends, watch HD videos, and surf the web simultaneously. With the Smart Alert feature, this smartphone alerts you of a missed call or text messages received in your absence. Furthermore, you can answer/reject a call, snooze the alarm or play music by simply giving out voice commands to this smartphone. Running on the Android 4.0 OS, this pebble blue Samsung Galaxy S III phone enables you to enjoy all applications on its 4.8-inch touchscreen.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/samsung_galaxy_s_iii_i9300_pebble_blue_506429593b259_7194133.jpg',
    1,
    575,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Samsung GalaxyS3 mini I8190',
    'With its gentle curves and comfortable grip, the Samsung Galaxy S III Mini continues the sophisticated design aesthetic seen in the Galaxy S III. Elegant natural design meets vivid viewing, thanks to a dazzling 4.0-inch Super AMOLED display that brings out the best in all your entertainment. With the Android 4.1 Jelly Bean operating system, you can enjoy a range of great apps and games as well as new functionality. And, with smart and intelligent services like Pop-up Play, Best Photo, Smart Stay and Social Tag, the Galaxy S III Mini helps make life easier and richer.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/samsung_galaxy_s_iii_mini_i8190_white_7292110.jpg',
    1,
    369,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Samsung Galaxy xCover S5690',
    'Powered by the 800 MHz Marvell MG2 processor, the Samsung Galaxy Xcover GT-S5690 smartphone allows you to easily multitask. Integrated with the Android v2.3 OS, this Samsung smartphone gives you access to various interesting applications. The 3.65-inch capacitive touchscreen of this Android phone enables you to navigate the menu with ease. Supporting 3G network, the Samsung Galaxy Xcover GT-S5690 ensures you get a speedy access to the web. Make extra room for your data by simply inserting a microSD card of up to 32 GB in the memory card slot of this Android phone. The 3.2 MP camera of this Samsung smartphone lets you capture good quality pictures.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/samsung_galaxy_xcover_s5690_6887287.jpg',
    1,
    499,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Sony Xperia Acro S 16GB White',
    'The Sony smartphone is powered by the Google Android operating system with several advanced features. The Sony XPERIA acro S comes with the Google Android 4.0 operating system, 1024 MB of RAM, and a fast 1500 MHz processor, all of which deliver an extremely robust mobile phone. The large 4.3-inch, 720p high-definition screen on this smartphone is scratch resistant, dustproof, and waterproof. The Sony smartphone has been unlocked so that it can be used with any wireless service provider. The Sony XPERIA acro S comes with 16 GB of storage space that may be upgraded with the addition of a microSD card. The 12.1 MP camera on this smartphone offers an LED flash, back-illuminated sensor, digital zoom, self-timing, and video recording at 1080p. The Sony smartphone also features video calling with its 1.3 MP front-facing camera. The Sony XPERIA acro S comes in white and has a candy-bar form factor. Additional features on the smartphone include Bluetooth 3.0, an HDMI port, Wi-Fi, and a microUSB.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/sony_xperia_acro_s_white_6654960.jpg',
    1,
    599,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Sony Xperia LTE 16 GB ion Black',
    'This LTE smartphone from the Sony XPERIA ion series delivers 4G network capabilities, a sizable amount of onboard memory, and powerful video capture, making it a high-powered multimedia phone. With 16 GB of internal storage and a 1.5 GHz dual-core central processing unit, the Sony XPERIA ion LTE cell phone has the processing power it needs to navigate today''s rich media environment. The two cameras on this smartphone feature 1080p and 720p HD video-capture capabilities, while the PlayStation certification that comes standard on this Sony XPERIA ion LTE cell phone makes it compatible with some of the most popular games available. The 4.6-inch display screen on this Sony XPERIA ion features HD Reality technology and gives users a display resolution of 1280x720 pixels. As an LTE smartphone, this Sony XPERIA ion works compatibly with the AT&T 4G service network wherever it is available, making it a strong performer amongst multimedia smartphones.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/sony_xperia_ion_black_6687959.jpg',
    1,
    399,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Sony Xperia Miro 4GB Black',
    'The Android 4.0 operating system offers thousands of apps and a sleek design, and the Sony XPERIA Miro offers the chance for cell phone users to take advantage of the popular operating system. Designed with a sleek black finish and a size small enough to fit almost anywhere, the Sony XPERIA miro black cell phone is both attractive and powerful. This smartphone boasts an 800 MHz Qualcomm SnapDragon processor and 512 MB of ram. The Sony XPERIA Miro even comes with an internal memory of 2.9 GB that is expandable all the way to 32 GB through a microSD card option. The Sony XPERIA miro black cell phone weighs only 110 grams and is constructed with a 3.5 inch display with a resolution of 320x480 pixels. This smartphone even provides users with a 5.0 MP camera with autofocus and LED flash for optimum performance. The Sony XPERIA Miro fives cell phone owners the chance to keep in their pocket a device that is a complete communication platform.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/sony_xperia_miro_black_4fd8973a5bb07_6680252.jpg',
    1,
    259,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Xperia Go 8GB Yellow',
    'The Sony smartphone is powered by Google Android, and it provides several robust features in a compact device. The Sony XPERIA go comes with the Android 4.0 Ice Cream Sandwich operating system and includes a variety of useful applications including Gmail, Google Maps, Picasa Web, Google Drive, and Google Maps Navigation. The smartphone has a candy-bar shape, dimensions of 4.37 inches high by 2.37 inches wide by 0.39 inches deep, and weighs less than 4 oz. The touchscreen on this Sony smartphone has a 320x480-pixel resolution and scratch-resistant glass. The Sony XPERIA go is a water-resistant device, which allows users to safely use it in the rain. The device comes with 8 GB of memory to store music, photos, and video files. Storage capacity can be upgraded by an additional 32 GB with use of a microSD storage card. The smartphone comes with a 1305 mAh battery that allows up to six hours of talktime. The Sony smartphone also comes with MP3 music ringtones, vibration mode, flight mode, silent mode, and a speakerphone. The Sony XPERIA go is unlocked for use with any carrier and comes in a signature yellow color.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/xperia_go_yellow_6653016.jpg',
    1,
    399,
    3
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Sony Xperia S 32GB White',
    'Access the Web at a lightening fast speed with the Sony Ericsson XPERIA S cell phone that supports 3G network. Operating this Sony Ericsson XPERIA is a breeze, thanks to its 4.3-inch capacitive touchscreen. The 12 MP camera of this 3G smartphone captures stunning, lifelike images. Powered by the Dual-core 1.5 GHz CPU, the Sony Ericsson XPERIA S cell phone delivers quick and efficient performance. Operating on the Android v2.3 OS, this Sony Ericsson XPERIA lets you access various Google applications like Google Maps, Gmail, GTalk, etc. Featuring a FM radio tuner, this 3G smartphone lets you tune in to your favorite radio channel anytime.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/2254b4a4069be46228260518d3cf42128195be0b/images/phones/xperia_s_white_6237614.jpg',
    1,
    639,
    3
  );

INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'ASUS Transformer Pad TF300TG',
    'Powered by the world''s first Quad-core processor NVIDIA® Tegra® 3 4-Plus-1™, the Transformer Pad browses the web and plays 1080p video at blazing speeds. The snappier response time and better multi-tasking performance make the Transformer Pad a true mobile entertainment powerhouse.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/5fd130d2a53a1c0943a4b7e44b8ce72c96fad7ee/images/tablets/ASUS_Transformer_Pad_TF300TG.jpg',
    10,
    660,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'ASUS Transformer Pad Infinity',
    'Powered by the fastest NVIDIA® Tegra® 3 4-Plus-1™ chip around, the world’s 1st quad-core mobile CPU blazes at 1.6GHz, providing the Transformer Pad Infinity the power to browse the web and play 1080p HD video at lightning fast speeds. The snappier response time and better multi-tasking performance make the Transformer Pad Infinity a true mobile entertainment powerhouse. ',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/5fd130d2a53a1c0943a4b7e44b8ce72c96fad7ee/images/tablets/ASUS_Transformer_Pad_Infinity_TF700T.jpg',
    5,
    770,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Amazon Kindle Fire HD 8.9',
    'Amazon’s latest Kindle Fire HD tablet may have just been released to the public, but that doesn’t mean Amazon isn’t going to put it on sale. It turns out that — for today only — the company is slashing $50 off the regular $299 starting price of the Kindle Fire HD 8.9, which means you can get the new tablet for only $249 for the 16GB version or $319 for the 32GB model.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/d4f4479e52e89dd19742ac0f8cc554f76e6f11af/images/tablets/Amazon_Kindle_Fire_HD_8.9.jpg',
    10,
    369,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Amazon Kindle Fire HD',
    'Most tablet displays are made up of two pieces of glass - an LCD on the bottom and a touch sensor on the top, separated by an air gap. With an air gap, light reflects off of every surface as it passes through from the front, creating multiple distracting reflections that reduce display contrast. Kindle Fire HD solves this air gap problem by laminating the touch sensor and the LCD together into a single layer of glass, creating a display that''s easy to view, even in overhead light.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/d4f4479e52e89dd19742ac0f8cc554f76e6f11af/images/tablets/Amazon_Kindle_Fire_HD.jpg',
    10,
    222,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'LG Optimus Pad',
    'Taking the tablet experience to an entirely new level, the LG Optimus Pad is powered by a dual core mobile processor and the made-for-tablet Android 3.0 Honeycomb Operating System. The 8.9 inch screen displays HD movies as they were meant to be seen, and the stereoscopic cameras capture 3D video to preserve your memories more lively.Experience the unparalleled power and performance that comes as standard with LG''s Optimus Pad Tablet PC. The perfection of the tablet form, the Optimus Pad has an 8.9 inch display that is sharp and bright, and displays HD video in all their cinematic glory. It is also the first tablet device to feature the powerful Dual Core Mobile processor and Android''s new Honeycomb OS, both of which will be new benchmarks for tablet excellence. The Optimus Pad also elevates the multimedia experience with dual stereoscopic cameras that record 3D video.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/d4f4479e52e89dd19742ac0f8cc554f76e6f11af/images/tablets/LG_Optimus_Pad.jpg',
    10,
    222,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Motorola Xoom 32Gb Wi-Fi',
    'Redefining the tablet by providing more ways to have fun, connect with friends and stay productive on the go, the Motorola XOOM boasts a dual core processor with each core running at 1 GHz, 10.1-inch widescreen HD display, and ultra-fast Wireless-N Wi-Fi networking. Winner of the Best of Show award at the 2011 Consumer Electronics Show (CES), the XOOM also offers support for Adobe Flash Player--enabling you to view the web without compromise.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/d4f4479e52e89dd19742ac0f8cc554f76e6f11af/images/tablets/Motorola_Xoom_32Gb_Wi-Fi.jpg',
    10,
    699,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Lenovo IdeaTab A2109',
    'The IdeaTab® A2109 is optimized for entertainment, a pleasure to use — and surprisingly affordable. From its real aluminum exterior and slim design to its quad-core NVIDIA® Tegra 3 processor, the A2109 looks great and performs even better. Powered by Android™ 4.0 Ice Cream Sandwich, the 9" A2109 is perfect for games, HD video, HD photos, and social media.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/d4f4479e52e89dd19742ac0f8cc554f76e6f11af/images/tablets/Lenovo_IdeaTab_A2109.jpg',
    10,
    289.96,
    2
  );

INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'ThinkPad Tablet 2 10.1-inch',
    'Three of the world''s leading technology companies collaborated to build the world''s leading tablet experience. Featuring the best that Lenovo®, Microsoft® and Intel® have to offer, the ThinkPad Tablet 2 redefines business, appeasing the IT manager and end user alike in one productive, mobile, reliable device.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/d4f4479e52e89dd19742ac0f8cc554f76e6f11af/images/tablets/ThinkPad_Tablet_2%2010.1-inch.jpg',
    10,
    699.99,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'ASUS VivoTab RT TF600T-B1-GR 10.1-Inch',
    '(VIVO RT) TF600T-B1-GR, Grey, 10.1" HD (1366x768) Super IPS+, OGS Touch Panel, Outdoor Readable Mode, NVIDIA Tegra 3 (1.3GHz), 2GB DDR3, NVIDIA Tegra 3, 32GB Flash, N/A, Win RT, 802.11BGN, 2MP & 8MP, 1 Year North America Warranty ',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/5fd130d2a53a1c0943a4b7e44b8ce72c96fad7ee/images/tablets/ASUS_Transformer_Pad_Infinity_TF700T.jpg',
    10,
    518.60,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Acer Iconia Tab A210-10g16u 10.1-Inch',
    'The Acer Iconia Tab A210 is a fun and practical 10.1" tablet with rich functionality that brings you closer to the things that matter most. This do it all Tablet lets you enjoy music, movies, gaming, reading, edit office documents, video calling and more. With powerful Quad-Core performance, the latest Android OS and a rich set of preloaded aps, this fun to use tablet is an amazing hub of activity for all ages, and it comes at a very affordable price! ',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/d4f4479e52e89dd19742ac0f8cc554f76e6f11af/images/tablets/Acer_Iconia_Tab_A210-10g16u_10.1-Inch.jpg',
    10,
    349.99,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Sony Xperia 16 GB 9.4-Inch',
    'Put unlimited entertainment in your hands. Xperia™ Tablet is the ultimate universal remote, with one-touch control of your TV, cable box and more.2 There''s even an exclusive app that puts a visual TV guide in your hands, fueled by your personal preferences and trending information from social media feeds. Enjoy music, movies, books, games and more. ',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/d4f4479e52e89dd19742ac0f8cc554f76e6f11af/images/tablets/Sony_Xperia_16_GB%209.4-Inch.jpg',
    10,
    349.99,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Archos 80 G9 Turbo ICS 8GB 8-Inch',
    'With its high-resolution, 8-inch display, the ARCHOS 80 G9 tablet is perfectly designed to give presentations, read e-magazines and e-books, or browse the web. It''s powered under the hood by a 1.5 GHz Texas Instruments OMAP4 multi-core ARM Cortex A9 processor--which gives you up to 50 percent faster performance than the Nvidia Tegra 2 processor found in many tablets. It also runs Android 4.0 (aka, Ice Cream Sandwich), which is specially designed and optimized for tablets.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/d4f4479e52e89dd19742ac0f8cc554f76e6f11af/images/tablets/Archos_80_G9_Turbo_ICS%208GB%208-Inch.jpg',
    10,
    204.26,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Archos 101b G9 Turbo ICS 8GB 10-Inch',
    'With its high-resolution, 10.1-inch display, the ARCHOS 101 G9 tablet is perfectly designed to give presentations, read e-magazines and e-books, or browse the web. It''s powered under the hood by a 1.5 GHz Texas Instruments OMAP4 multi-core ARM Cortex A9 processor--which gives you up to 50 percent faster performance than the Nvidia Tegra 2 processor found in many tablets. It also runs Android 4.0 (aka, Ice Cream Sandwich), which is specially designed and optimized for tablets.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/d4f4479e52e89dd19742ac0f8cc554f76e6f11af/images/tablets/Archos_101_G9_Turbo_ICS_8GB_10-Inch.jpg',
    10,
    279.99,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Archos ARNOVA 9 G2 4GB 9.7-Inch',
    'Powered by a super-fast 1 GHz processor, the ARNOVA 9 G2 comes with an ultra-responsive multi-touch screen and optimized Android 2.3 Gingerbread operating system. This smart combination between high performance hardware and intuitive software will offer you many ways to use your tablet. Browse the web smoother than ever before and play HD videos in Full HD 1080 quality.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/d4f4479e52e89dd19742ac0f8cc554f76e6f11af/images/tablets/Archos_ARNOVA_9_G2_4GB_9.7-Inch.jpg',
    10,
    169.97,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'ARCHOS 101 Internet',
    'Offering a unique tablet design with the latest Android operating system, the ARCHOS 101 Internet tablet features a 10.1-inch multi-touch display with pinch-and-zoom capabilities, a fast 1. GHz processor for fast web browsing, HD multimedia playback, and multitasking across applications. With the Android 2.2 OS (dubbed "Froyo"), the ARCHOS 101 provides support for the Adobe Flash 10.1 player, enabling you to view the web and streaming video sites without compromise.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/d4f4479e52e89dd19742ac0f8cc554f76e6f11af/images/tablets/ARCHOS_101_Internet.jpg',
    10,
    205.99,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Archos 5 16GB Internet',
    'ARCHOS, award-winning for its constant technology innovation, now introduces its new ARCHOS 5 Internet Tablet. This product combines all the multimedia know-how that ARCHOS is famous for, with the ANDROID operating system. It offers you an uncompromised Internet, media, and TV experience in a customizable interface. In perpetual evolution with the continuous arrival of new applications, you can personalize your ARCHOS 5 Internet Tablet to perfectly reflect your tastes and needs.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/4bc89dbe2d06581fe3d73a37ea8ecc9a8315c75e/images/tablets/Archos_5_16GB_Internet.jpg',
    10,
    205.99,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Archos 7 8GB Home',
    'ARCHOS'' new ARCHOS 7 Home Tablet bridges the gap between the smartphone and the desktop PC, providing constant access to the web, customization through Android Apps, and delivery of multimedia content--all in a seven-inch widescreen format. Access the information you need, such as news, weather, or local business info. Enjoy your favorite movie clips or watch some YouTube videos for a quick entertainment fix. Or just kick back and listen to some music. With built-in Wi-Fi, quick and easy file transfer, and up to 7 hours video/44 hours music on a single charge, this affordable tablet will keep you entertained--and connected--all day long.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/4bc89dbe2d06581fe3d73a37ea8ecc9a8315c75e/images/tablets/Archos_7_8GB_Home.jpg',
    10,
    149.99,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Acer Iconia W510-1666 10.1-Inch',
    'The 10.1" Acer ICONIA W510 is an ultra-responsive HD tablet that delivers an incredible hands-on experience in a lightweight device that''s smaller than a magazine. Featuring an Intel® Atom™ Processor, the W510 runs on the new Windows 8 operating system and sports a 5-point multi-touch display that opens up new possibilities for interaction. This easy to carry tablet starts up with one press like a smartphone, while up to 9 hours of battery life keep you at peak productivity.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/4bc89dbe2d06581fe3d73a37ea8ecc9a8315c75e/images/tablets/Acer_Iconia_W510-1666_10.1-Inch.jpg',
    10,
    599.99,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Acer Iconia W700-6607 11.6-Inch',
    'The Acer ICONIA W700 blends Ultrabook performance and tablet mobility with Windows 8 touch capabilities for hands-on multimedia in brilliant clarity. Grab your tablet to go or attach it to the dock and use as a full function PC with the included Bluetooth keyboard. It also acts as the perfect stand for sharing or viewing content as you view, touch and browse on demand. And with 9 hours of battery life, your digital life is always just a touch away.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/4bc89dbe2d06581fe3d73a37ea8ecc9a8315c75e/images/tablets/Acer_Iconia_W700-6607%20_1.6-Inch.jpg',
    10,
    799.99,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Acer Iconia Tab A110-07g08u 7-Inch',
    'With a 7” display and less than 0.85 lbs. of weight, Acer Iconia Tab A110 is more compact to carry than a 10” device, and more comfortable to browse than a smartphone. The pocketable yet powerful quad-core platform with Jelly Bean OS is capable of satisfying your needs for reading, browsing, multimedia enjoyment, and organizing your day for your daily on-the-go life. ',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/4bc89dbe2d06581fe3d73a37ea8ecc9a8315c75e/images/tablets/Acer_Iconia_Tab_A110-07g08u%207-Inch.jpg',
    10,
    229.99,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Dell XPS 10 XPS10-2727BLK 10.1-Inch',
    'The XPS™ 10 tablet is light, slender and crafted with resilient materials so it''s ready to go wherever you go.Experience Windows® RT on the XPS 10 with apps for work and play. Get familiar with Microsoft® Office Home and Student 2013 RT and see how your favourite productivity suite has been optimised for touch.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/4bc89dbe2d06581fe3d73a37ea8ecc9a8315c75e/images/tablets/Dell_XPS_10_XPS10-2727BLK_10.1-Inch.jpg',
    10,
    229.99,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Coby Kyros 7-Inch',
    'Connect to the world with the touch of a fingertip. The Coby Kyros MID7048-4 tablet is a powerful and flexible internet and entertainment solution with a 7-inch capacitive touch screen and intuitive Android 4.0 operating system (Ice-Cream Sandwich). Get access to thousands of applications from GetJar marketplace with plenty of games, social media, and productivity tools to meet your needs. With sleek and stylish Coby tablets, you can relax and enjoy your favorite music, 1080P HD videos, e-Books and photos. Enjoy doing more with Coby''s tablets.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/4bc89dbe2d06581fe3d73a37ea8ecc9a8315c75e/images/tablets/Coby_Kyros_7-Inch.jpg',
    10,
    92.99,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Le Pan I TC 970 9.7-Inch',
    'Le-Pan TC970 Tablet offers a slim design and intuitive touch navigation via its bright 9.7-inch display. With the Android 2.2 OS (Froyo), the tablet offers full support for Adobe Flash Player 10.2 for accessing Flash-enabled sites, watching 720P video and playing games. This tablet weights 1.6 pounds and just 0.48 inches thin, Le Pan Tablet has a super slim and sleek design and feels very comfortable in your hands with its smooth beveled edges. It''s great for multitasking, viewing multiple apps, browsing entire web pages, and sharing entertainment and photos.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/4bc89dbe2d06581fe3d73a37ea8ecc9a8315c75e/images/tablets/Le_Pan_I_TC%20970_9.7-Inch.jpg',
    10,
    149.99,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Arnova 7 G2 4GB',
    'Experience smooth performance with the Arnova 7 G2 tablet, whether you''re listening to music, playing video games, or watching HD videos. The device runs Google''s Android 2.3 Gingerbread operating system and is powered by a fast 1 GHz processor. With capacitive touch technology and a 7-inch, 800-by-480-pixel screen, it''s great for surfing the web. You''ll also have access to the Arnova AppsLib store, which offers thousands of Android-based apps.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/4bc89dbe2d06581fe3d73a37ea8ecc9a8315c75e/images/tablets/Arnova_7_G2_4GB.jpg',
    10,
    89.99,
    2
  );
INSERT INTO Products (name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    'Archos ChildPad 7-Inch',
    'Offering an improved 7-inch capacitive touchscreen display, the ARNOVA ChildPad AV from Archos provides hours of entertainment for users of all ages. It features a kid-friendly user interface with colored icons, advanced parental control app, and the latest version of the Android operating system--Ice Cream Sandwich (4.0).',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/4bc89dbe2d06581fe3d73a37ea8ecc9a8315c75e/images/tablets/Archos_ChildPad_7-Inch.jpg',
    10,
    139.99,
    2
  );