package com.exadel.i.webshop.dao;

import com.exadel.i.webshop.domain.Order;
import com.exadel.i.webshop.domain.OrderLine;
import com.exadel.i.webshop.domain.OrderStatus;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ksenia Orlenko
 */
@Repository
public class OrderDAO {
    private final Log log = LogFactory.getLog(OrderDAO.class);
    private static final OrderMapper orderMapper = new OrderMapper();
    private static final OrderLineMapper orderLineMapper = new OrderLineMapper();

    @Resource
    private JdbcTemplate template;

    @Transactional
    public Long create(Order order) {
        Long id = generateId();
        template.update("INSERT INTO Orders (id,orderDate, orderStatus, userId,billingAddressId,shippingAddressId,total,phoneNumber) VALUES (?,?,?,?,?,?,?,?)",
                new Object[]{id, order.getOrderDate(), order.getOrderStatus().getValue(), order.getUserId(), order.getBillingAddressId(),
                        order.getShippingAddressId(), order.getTotal(), order.getPhoneNumber()});
        log.info("Order created. ID="+id);
        return id;
    }

    @Transactional(readOnly = true)
    public Order findById(Long id) {
        return template.queryForObject("select * from Orders where id = ?", new Object[]{id}, orderMapper);
    }

    @Transactional(readOnly = true)
    public List<Order> findAll() {
        return template.query("select * from Orders", orderMapper);
    }

    @Transactional(readOnly = true)
    public List<Order> findByUserId(Long id) {
        return template.query("select * from Orders where userId = ?", new Object[]{id}, orderMapper);
    }

    @Transactional(readOnly = true)
    public OrderLine findByProductId(Long productId) {
        return template.queryForObject("select * from OrderLines where productId= ?", new Object[]{productId}, orderLineMapper);
    }

    @Transactional
    public void createOrderLine(OrderLine orderLine) {
        template.update("INSERT INTO OrderLines (orderId, productId, quantity) VALUES (?,?,?)",
                new Object[]{orderLine.getOrderId(), orderLine.getProductId(), orderLine.getQuantity()});
    }

    public Long generateId() {
        return template.queryForLong("CALL NEXT VALUE FOR ORDER_SEQUENCE");
    }

    @Transactional
    public void deleteOrder(Long id) {
        template.update("DELETE FROM Orders WHERE id = ?", new Object[]{id});
        log.info("Order deleted. ID="+id);
    }

    @Transactional
    public void deleteOrderLine(Long id) {
        template.update("DELETE FROM OrderLines WHERE id = ?", new Object[]{id});
    }

    @Transactional
    public void increaseOrderLineQuantity(Long id) {
        template.update("UPDATE OrderLines SET quantity=quantity+1 WHERE id=?", new Object[]{id});
    }

    @Transactional
    public void decreaseOrderLineQuantity(Long id) {
        template.update("UPDATE OrderLines SET quantity=quantity-1 WHERE id=?", new Object[]{id});
    }

    @Transactional(readOnly = true)
    public Order findByIdWithOrderLines(Long id) {
        String sql = "SELECT o.id, o.orderDate, o.orderStatus, o.total, o.billingAddressId, o.shippingAddressId,o.userId,o.phoneNumber, ol.id " +
                "AS orderWithLineId, ol.productId, ol.quantity,ol.orderId FROM Orders o LEFT JOIN OrderLines ol ON o.id = ol.orderId WHERE o.id=?";
        return template.query(sql, new Object[]{id}, new OrderWithOrderLineExtractor());
    }

    @Transactional
    public void updateOrderTotal(Long id, Double total) {
        template.update("UPDATE Orders SET total=? WHERE id= ?", new Object[]{total, id});
    }

    @Transactional
    public void updateOrderStatus(Long id, OrderStatus status) {
        template.update("UPDATE Orders SET orderStatus=? WHERE id= ?", new Object[]{status.getValue(), id});
    }

    private static final class OrderWithOrderLineExtractor implements ResultSetExtractor<Order> {

        @Override
        public Order extractData(ResultSet rs) throws SQLException, DataAccessException {
            Order order = null;
            while (rs.next()) {
                Long id = rs.getLong("id");
                if (order == null) {
                    order = new Order();
                    order.setId(id);
                    order.setOrderDate(rs.getDate("orderDate"));
                    order.setOrderStatus(OrderStatus.fromInt(rs.getInt("orderStatus")));
                    order.setUserId(rs.getLong("userId"));
                    order.setBillingAddressId(rs.getLong("billingAddressId"));
                    order.setShippingAddressId(rs.getLong("shippingAddressId"));
                    order.setTotal(rs.getDouble("total"));
                    order.setOrderLines(new ArrayList<OrderLine>());
                    order.setPhoneNumber(rs.getString("phoneNumber"));
                }
                Long orderWithLineId = (Long) rs.getObject("orderWithLineId");
                if (orderWithLineId != null) {
                    OrderLine orderLine = new OrderLine();
                    orderLine.setId(rs.getLong("orderWithLineId"));
                    orderLine.setOrderId(id);
                    orderLine.setOrderId(rs.getLong("orderId"));
                    orderLine.setProductId(rs.getLong("productId"));
                    orderLine.setQuantity(rs.getInt("quantity"));
                    order.getOrderLines().add(orderLine);
                }
            }
            return order;

        }
    }

    private static final class OrderLineMapper implements RowMapper<OrderLine> {
        @Override
        public OrderLine mapRow(ResultSet rs, int i) throws SQLException {
            OrderLine orderLine = new OrderLine();
            orderLine.setId(rs.getLong("id"));
            orderLine.setOrderId(rs.getLong("orderId"));
            orderLine.setProductId(rs.getLong("productId"));
            orderLine.setQuantity(rs.getInt("quantity"));
            return orderLine;
        }
    }

    private static final class OrderMapper implements RowMapper<Order> {
        @Override
        public Order mapRow(ResultSet rs, int i) throws SQLException {
            Order order = new Order();
            order.setId(rs.getLong("id"));
            order.setOrderDate(rs.getDate("orderDate"));
            order.setOrderStatus(OrderStatus.fromInt(rs.getInt("orderStatus")));
            order.setUserId(rs.getLong("userId"));
            order.setBillingAddressId(rs.getLong("billingAddressId"));
            order.setShippingAddressId(rs.getLong("shippingAddressId"));
            order.setTotal(rs.getDouble("total"));
            order.setPhoneNumber(rs.getString("phoneNumber"));
            return order;
        }
    }
}
