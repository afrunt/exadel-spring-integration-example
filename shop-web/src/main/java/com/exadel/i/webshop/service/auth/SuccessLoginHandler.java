package com.exadel.i.webshop.service.auth;

import com.exadel.i.webshop.dao.UserDAO;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Andrey Frunt
 */
@Service
public class SuccessLoginHandler extends SavedRequestAwareAuthenticationSuccessHandler {
    @Resource
    private UserDAO userDAO;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
        String email = authentication.getName();
        request.getSession().setAttribute("CURRENT_USER", userDAO.findByEmail(email));
        super.onAuthenticationSuccess(request, response, authentication);
    }
}
