package com.exadel.i.webshop.dao;

import com.exadel.i.webshop.domain.Category;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Andrey Frunt
 */
@Repository
public class CategoryDAO {
    private static final CategoryMapper mapper = new CategoryMapper();
    @Resource
    private JdbcTemplate template;

    public Category findFirst() {
        return template.queryForObject("SELECT * FROM Categories ORDER BY name LIMIT 1", mapper);
    }

    public List<Category> findAll() {
        return template.query("SELECT * FROM Categories ORDER BY name", mapper);
    }

    public Category findById(Long categoryId) {
        return template.queryForObject("SELECT * FROM Categories WHERE id=?", new Object[]{categoryId}, mapper);
    }

    private static class CategoryMapper implements RowMapper<Category> {

        @Override
        public Category mapRow(ResultSet rs, int i) throws SQLException {
            Category c = new Category();
            c.setId(rs.getLong("id"));
            c.setName(rs.getString("name"));
            return c;
        }
    }

}
