package com.exadel.i.webshop.domain;

/**
 * @author Andrey Frunt
 */
public class ShippingAddress extends AbstractAddress {
    private Long userId;
    private String zip;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Override
    public String toString() {
        return getAddress() + "," + getCity() + "," + getState() + "," + getZip();
    }
}
