package com.exadel.i.webshop.service.exception;

/**
 * @author Andrey Frunt
 */
public abstract class AbstractServiceException extends Exception {
}
