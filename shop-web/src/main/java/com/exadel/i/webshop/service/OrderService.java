package com.exadel.i.webshop.service;

import com.exadel.i.webshop.dao.BillingAddressDAO;
import com.exadel.i.webshop.dao.OrderDAO;
import com.exadel.i.webshop.dao.ProductDAO;
import com.exadel.i.webshop.dao.ShippingAddressDAO;
import com.exadel.i.webshop.domain.*;
import com.exadel.i.webshop.service.exception.NotEnoughProductsException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Frunt
 */
@Service
public class OrderService {

    private static final Log log = LogFactory.getLog(OrderService.class);

    public static final String NOT_ENOUGH_PRODUCTS = "Not enough products available to complete your order. We have only {0} units of {1}";

    @Resource
    private OrderDAO orderDAO;
    @Resource
    private ProductDAO productDAO;
    @Resource
    private BillingAddressDAO billingAddressDAO;
    @Resource
    private ShippingAddressDAO shippingAddressDAO;

    @Resource(name = "newOrderOutputChannel")
    private MessageChannel channel;

    @Resource(name = "packagingOrderOutputChannel")
    private MessageChannel packagingChannel;

    /**
     * Save order to database and send message with order to revise module
     *
     * @param cart              - shopping cart
     * @param userId            - user id
     * @param phoneNumber       - order phone number
     * @param billingAddressId  - order billing address
     * @param shippingAddressId - order shipping address
     * @return - order id
     * @throws NotEnoughProductsException - throws if not enough available product
     */
    @Transactional(rollbackFor = NotEnoughProductsException.class)
    public long submitOrder(ShoppingCart cart, Long userId, String phoneNumber, Long billingAddressId, Long shippingAddressId) throws NotEnoughProductsException {
        log.info("Submitting order");
        Order order = new Order();
        order.setOrderDate(new Date());
        order.setOrderStatus(OrderStatus.NEW);
        order.setUserId(userId);
        order.setBillingAddressId(billingAddressId);
        order.setShippingAddressId(shippingAddressId);
        order.setTotal(cart.getTotal());
        order.setPhoneNumber(phoneNumber);
        order.setOrderLines(new ArrayList<OrderLine>());
        Map<Long, Product> productsMap = ProductDAO.asMap(productDAO.findByIds(cart.getProductIds()));
        List<CartItem> cartItems = cart.getItems();
        validateItems(productsMap, cartItems);
        Long orderId = orderDAO.create(order);
        log.info("Save order with id:" + orderId);
        for (CartItem cartItem : cartItems) {
            Product product = productsMap.get(cartItem.getProductId());
            Long countAvailable = product.getCountAvailable();
            if (countAvailable <= 0 || countAvailable < cartItem.getQuantity()) {
                String message = MessageFormat.format(NOT_ENOUGH_PRODUCTS, countAvailable, product.getName());
                log.error(message);
                throw new NotEnoughProductsException(countAvailable, product.getName());
            }
            OrderLine orderLine = new OrderLine();
            orderLine.setOrderId(orderId);
            orderLine.setProductId(cartItem.getProductId());
            orderLine.setQuantity(cartItem.getQuantity());
            orderDAO.createOrderLine(orderLine);
            log.info("Save order line of order with id:" + orderId);
            productDAO.decreaseAvailableCount(cartItem.getProductId(), cartItem.getQuantity());
            log.info("Decrease into " + cartItem.getQuantity() + " available count of product with id:" + cartItem.getProductId());
        }
        OrderMessage orderMessage = createOrderMessage(orderId);
        channel.send(MessageBuilder.withPayload(orderMessage).build());
        log.info("Send message order with id:" + orderMessage.getId() + " to revise");
        orderDAO.updateOrderStatus(orderId, OrderStatus.REVISING);
        log.info("Update order status to revising");
        log.info("Order submitted. ID=" + orderId);
        return orderId;
    }

    /**
     * Validate count of products in shopping cart
     *
     * @param productsMap - map of product
     * @param cartItems   - cart line
     * @throws NotEnoughProductsException - throws if not enough available product
     */
    private void validateItems(Map<Long, Product> productsMap, List<CartItem> cartItems) throws NotEnoughProductsException {
        for (CartItem cartItem : cartItems) {
            Product product = productsMap.get(cartItem.getProductId());
            Long countAvailable = product.getCountAvailable();
            if (countAvailable <= 0 || countAvailable < cartItem.getQuantity()) {
                String message = MessageFormat.format(NOT_ENOUGH_PRODUCTS, countAvailable, product.getName());
                log.error(message);
                throw new NotEnoughProductsException(countAvailable, product.getName());
            }
        }
    }

    /**
     * Convert Order to OrderMessage
     *
     * @param orderId - order id
     * @return - OrderMessage
     */
    private OrderMessage createOrderMessage(Long orderId) {
        log.info("Converting order to order message");
        Order order = orderDAO.findByIdWithOrderLines(orderId);
        OrderMessage orderMessage = new OrderMessage();
        orderMessage.setId(order.getId());
        orderMessage.setPhoneNumber(order.getPhoneNumber());
        orderMessage.setTotal(order.getTotal());
        BillingAddress billingAddress = billingAddressDAO.findById(order.getBillingAddressId());
        orderMessage.setBillingAddress(billingAddress.toString());
        ShippingAddress shippingAddress = shippingAddressDAO.findById(order.getShippingAddressId());
        orderMessage.setShippingAddress(shippingAddress.toString());
        List<OrderLine> orderLines = order.getOrderLines();
        List<Long> productIds = new ArrayList<Long>(orderLines.size());
        for (OrderLine orderLine : orderLines) {
            productIds.add(orderLine.getProductId());
        }
        Map<Long, Product> productsMap = ProductDAO.asMap(productDAO.findByIds(productIds));
        ArrayList<OrderMessage.MessageOrderLine> messageOrderLines = new ArrayList<OrderMessage.MessageOrderLine>();
        for (OrderLine orderLine : orderLines) {
            OrderMessage.MessageOrderLine messageOrderLine = new OrderMessage.MessageOrderLine();
            messageOrderLine.setId(orderLine.getId());
            messageOrderLine.setProductId(orderLine.getProductId());
            Product product = productsMap.get(orderLine.getProductId());
            messageOrderLine.setName(product.getName());
            messageOrderLine.setPrice(product.getPrice());
            messageOrderLine.setQuantity(orderLine.getQuantity());
            messageOrderLine.setImageUrl(product.getImageUrl());
            messageOrderLines.add(messageOrderLine);
        }
        orderMessage.setOrderLines(messageOrderLines);
        log.info("Converted order to order message");
        return orderMessage;
    }

    /**
     * Receive reject message form revise module
     *
     * @param orderId - rejected order id
     */
    @Transactional
    public void rejectOrder(long orderId) {
        log.info("Receive reject message form revise module");
        log.info("Rejecting order with id:" + orderId);
        Order order = orderDAO.findByIdWithOrderLines(orderId);
        if (order != null) {
            List<OrderLine> orderLines = order.getOrderLines();
            for (OrderLine orderLine : orderLines) {
                productDAO.increaseAvailableCount(orderLine.getProductId(), orderLine.getQuantity());
                log.info("Increase into " + orderLine.getQuantity() + " available count of product with id:" + orderLine.getProductId());
            }
            orderDAO.deleteOrder(orderId);
            log.info("Delete order with id:" + order.getId());
            log.info("Rejected order with id:" + orderId);
        }
    }

    /**
     * Remove product from order
     *
     * @param orderId   - order id
     * @param productId - product id
     */
    @Transactional
    public void removeProductFromOrder(long orderId, long productId) {
        log.info("Deleting product with id:" + productId + " from order with id:" + orderId);
        Product product = productDAO.findById(productId);
        Order order = orderDAO.findByIdWithOrderLines(orderId);
        if (order != null) {
            OrderLine orderLineWithProduct = findOrderLineWithProduct(order.getOrderLines(), productId);
            if (orderLineWithProduct != null) {
                orderDAO.deleteOrderLine(orderLineWithProduct.getId());
                log.info("Deleter order line with id:" + orderLineWithProduct.getId());
                productDAO.increaseAvailableCount(productId, orderLineWithProduct.getQuantity());
                log.info("Increase into " + orderLineWithProduct.getQuantity() + " available count of product with id:" + productId);
                Double total = decreaseTotal(order.getTotal(), product.getPrice(), orderLineWithProduct.getQuantity());
                orderDAO.updateOrderTotal(order.getId(), total);
                log.info("Update order total to " + total);
                log.info("Deleted product with id:" + productId + " from order with id:" + orderId);
            }
        }
    }

    /**
     * Add product to order
     *
     * @param orderId   - order id
     * @param productId - product id
     * @throws NotEnoughProductsException - throws if not enough available product
     */
    @Transactional
    public void addProductToOrder(long orderId, long productId) throws NotEnoughProductsException {
        log.info("Adding product with id:" + productId + " to order with id:" + orderId);
        Product product = productDAO.findById(productId);
        if (product.getCountAvailable() <= 0) {
            String message = MessageFormat.format(NOT_ENOUGH_PRODUCTS, product.getCountAvailable(), product.getName());
            log.error(message);
            throw new NotEnoughProductsException(product.getCountAvailable(), product.getName());
        }
        Order order = orderDAO.findByIdWithOrderLines(orderId);
        OrderLine orderLineWithProduct = findOrderLineWithProduct(order.getOrderLines(), productId);
        if (orderLineWithProduct != null) {
            orderDAO.increaseOrderLineQuantity(orderLineWithProduct.getId());
            log.info("Increase order line quantity into " + orderLineWithProduct.getQuantity());
        } else {
            OrderLine orderLine = new OrderLine();
            orderLine.setOrderId(order.getId());
            orderLine.setProductId(productId);
            orderLine.setQuantity(1);
            orderDAO.createOrderLine(orderLine);
            log.info("Save order line of order with id:" + orderId);
        }
        productDAO.decreaseAvailableCount(productId, 1);
        log.info("Decrease into 1 available count of product with id:" + productId);
        Double total = increaseTotal(order.getTotal(), product.getPrice(), 1);
        orderDAO.updateOrderTotal(order.getId(), total);
        log.info("Update order total to " + total);
        log.info("Added product with id:" + productId + " to order with id:" + orderId);
    }

    private Double decreaseTotal(Double total, Double price, int quantity) {
        total = total - price * quantity;
        return total;
    }

    private Double increaseTotal(Double total, Double price, int quantity) {
        total = total + price * quantity;
        return total;
    }

    private OrderLine findOrderLineWithProduct(List<OrderLine> orderLines, Long productId) {
        for (OrderLine line : orderLines) {
            if (productId.equals(line.getProductId())) {
                return line;
            }
        }
        return null;
    }

    /**
     * Receive revise message form revise module
     *
     * @param id - order id
     */
    @Transactional
    public void reviseOrder(Long id) {
        log.info("Receive revise message form revise module");
        log.info("Revising order with id:" + id);
        orderDAO.updateOrderStatus(id, OrderStatus.REVISED);
        log.info("Update order status to revised");
        OrderMessage orderMessage = createOrderMessage(id);
        packagingChannel.send(MessageBuilder.withPayload(orderMessage).build());
        log.info("Send message order with id:" + orderMessage.getId() + " to packaging module");
        orderDAO.updateOrderStatus(id, OrderStatus.PACKAGING);
        log.info("Update order status to packaging");
        log.info("Revised order with id:" + id);
    }

    /**
     * Receive packaging message form revise module
     *
     * @param id - order id
     */
    @Transactional
    public void packageOrder(Long id) {
        log.info("Receive packaging message form revise module");
        orderDAO.updateOrderStatus(id, OrderStatus.DELIVERING);
        log.info("Update order status to delivering");
        log.info("Order delivered");
    }
}
