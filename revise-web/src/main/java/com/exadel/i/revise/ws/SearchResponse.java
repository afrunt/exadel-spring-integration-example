package com.exadel.i.revise.ws;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrey Frunt
 */
@XmlRootElement
public class SearchResponse implements Serializable {
    private List<SearchResponseItem> items = new ArrayList<SearchResponseItem>();

    @XmlElement(name = "items")
    public List<SearchResponseItem> getItems() {
        return items;
    }

    public void setItems(List<SearchResponseItem> items) {
        this.items = items;
    }

    @XmlRootElement
    public static class SearchResponseItem implements Serializable {
        private Long productId;
        private String name;
        private Long countAvailable;
        private double price;
        private String imageUrl;

        public Long getProductId() {
            return productId;
        }

        public void setProductId(Long productId) {
            this.productId = productId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getCountAvailable() {
            return countAvailable;
        }

        public void setCountAvailable(Long countAvailable) {
            this.countAvailable = countAvailable;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }
    }
}
