package com.exadel.i.webshop.dao;

import com.exadel.i.webshop.domain.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Andrey Frunt
 */
@Repository
public class UserDAO {
    private static final UserMapper mapper = new UserMapper();
    @Resource
    private JdbcTemplate template;

    public User findById(Long id) {
        return template.queryForObject("SELECT * FROM Users WHERE id = ?", new Object[]{id}, mapper);
    }

    public List<User> findAll() {
        return template.query("SELECT * FROM Users", mapper);
    }

    public User findByEmail(String email) {
        return template.queryForObject("SELECT * FROM Users WHERE email=?", new Object[]{email}, mapper);
    }

    private static final class UserMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int i) throws SQLException {
            User user = new User();
            user.setId(rs.getLong("id"));
            user.setFirstName(rs.getString("firstName"));
            user.setLastName(rs.getString("lastName"));
            user.setEmail(rs.getString("email"));
            user.setPasswordHash(rs.getString("passwordHash"));
            user.setPhoneNumber(rs.getString("phoneNumber"));
            return user;
        }
    }
}
