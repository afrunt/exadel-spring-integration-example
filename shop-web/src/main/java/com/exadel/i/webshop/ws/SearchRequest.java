package com.exadel.i.webshop.ws;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author Andrey Frunt
 */
@XmlRootElement
public class SearchRequest implements Serializable {
    private String searchTerm;

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }
}
