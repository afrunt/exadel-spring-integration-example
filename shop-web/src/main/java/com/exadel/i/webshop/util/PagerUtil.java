package com.exadel.i.webshop.util;

/**
 * @author Andrey Frunt
 */
public final class PagerUtil {
    public static final int DEFAULT_PER_PAGE = 4;

    public static int calculatePageCount(int totalCount, int perPage, int defaultPerPage) {
        if (perPage <= 0) {
            perPage = defaultPerPage;
        }

        int pageCount = totalCount / perPage;
        if (onePageLeft(totalCount, perPage)) {
            ++pageCount;
        }

        return pageCount;
    }

    public static int calculatePageCount(int totalCount, int perPage) {
        return calculatePageCount(totalCount, perPage, DEFAULT_PER_PAGE);
    }

    public static int calculateOffset(int totalCount, int pageNo, int pageCount, int perPage) {
        int offset = pageNo * perPage;
        if (offset >= totalCount) {
            if (offset != 0) {
                --offset;
            }
        }
        return offset;
    }

    public static int calculateOffset(int totalCount, int pageNo, int pageCount) {
        if (pageNo >= pageCount) {
            pageNo = pageCount - 1;
        }
        if (pageNo < 0) {
            pageNo = 0;
        }
        return calculateOffset(totalCount, pageNo, pageCount, DEFAULT_PER_PAGE);
    }

    public static int calculateOffset(int totalCount, int pageNo) {
        return calculateOffset(totalCount, pageNo, calculatePageCount(totalCount, DEFAULT_PER_PAGE));
    }

    private static boolean onePageLeft(int totalCount, int perPage) {
        return (long) totalCount % (long) perPage > 0;
    }
}
