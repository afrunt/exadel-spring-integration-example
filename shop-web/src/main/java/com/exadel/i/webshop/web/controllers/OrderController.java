package com.exadel.i.webshop.web.controllers;

import com.exadel.i.webshop.dao.CategoryDAO;
import com.exadel.i.webshop.dao.OrderDAO;
import com.exadel.i.webshop.dao.ProductDAO;
import com.exadel.i.webshop.domain.Order;
import com.exadel.i.webshop.domain.OrderLine;
import com.exadel.i.webshop.domain.Product;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Ksenia Orlenko
 */
@Controller
@RequestMapping("/order")
public class OrderController {
    @Resource
    OrderDAO orderDAO;
    @Resource
    CategoryDAO categoryDAO;
    @Resource
    ProductDAO productDAO;

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = {"/{id}", "/{id}/"}, method = RequestMethod.GET)
    public String show(@PathVariable Long id, Model uiModel, HttpSession session) {
        Order order = orderDAO.findByIdWithOrderLines(id);
        if (order != null) {
            uiModel.addAttribute("order", order);
            List<OrderLine> orderLines = order.getOrderLines();
            if (!orderLines.isEmpty()) {
                List<Long> productIds = new ArrayList<Long>(orderLines.size());
                for (OrderLine orderLine : orderLines) {
                    productIds.add(orderLine.getProductId());
                }
                Map<Long, Product> productsMap = ProductDAO.asMap(productDAO.findByIds(productIds));
                uiModel.addAttribute("products", productsMap);
            }
            uiModel.addAttribute("categories", categoryDAO.findAll());
            return "order-show";
        }
        return "redirect:/profile/";
    }
}
