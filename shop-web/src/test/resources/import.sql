INSERT INTO Roles (id, name) VALUES (1, 'USER');
INSERT INTO Roles (id, name) VALUES (2, 'ADMIN');

INSERT INTO Users (id, firstName, lastName, email, passwordHash,phoneNumber)
  VALUES (1, 'John', 'Doe', 'john.doe@gmail.com', md5('test', 'UTF-8'),'01234567899');


INSERT INTO Users (id, firstName, lastName, email, passwordHash,phoneNumber)
  VALUES (2, 'Alex', 'Doe', 'alex.doe@gmail.com', md5('test', 'UTF-8'),'12345678910');

INSERT INTO Users (id, firstName, lastName, email, passwordHash,phoneNumber)
  VALUES (3, 'Jess', 'Doe', 'jess.doe@gmail.com', md5('test', 'UTF-8'),'23456789102');

INSERT INTO UserRoles (userId, roleId) VALUES (1, 1);

INSERT INTO ShippingAddresses (id,userId, name, state, city, address, zip)
  VALUES (1,1, 'John Doe', 'CA', 'Walnut Creek', '1340 Treat Blvd.', '94597');
INSERT INTO BillingAddresses (id,userId, name, state, city, address)
  VALUES (1,1, 'John Doe', 'CA', 'Walnut Creek', '1340 Treat Blvd.');

INSERT INTO Categories (id, name) VALUES (1, 'Laptops');
INSERT INTO Categories (id, name) VALUES (2, 'Tablets');
INSERT INTO Categories (id, name) VALUES (3, 'Phones');

INSERT INTO Products (id, name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    1,
    'Fujitsu Lifebook AH512',
    'The Fujitsu Lifebook AH512 is a solid everyday notebook with a high-definition 15.6 inch widescreen display. Additionally you can enjoy all your media on a TV via HDMI. Integrated WLAN and Bluetooth provide you with great connectivity and the spill-resistant keyboard with number pad provides extra reliability and usability.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/fecd389225fa81a5851faf84f87eb66a38876cc2/images/fujitsu_lifebook_ah512_vfy_ah512mpao5ru_7692928.jpg',
    50,
    559,
    1
  );

INSERT INTO Products (id, name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    2,
    'HP ProBook 4540s (B6N37EA)',
    'Your Business Partner. Optimized for Windows® 7, these notebooks are ideal for SMBs. They offer multimedia tools, easy-to-use security and a sleek, vertical brushed aluminum casing. Options include discrete graphics and battery-saving mode, and two display sizes.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/fecd389225fa81a5851faf84f87eb66a38876cc2/images/hp_probook_4540s_b6n37ea_6784406.jpg',
    5,
    670,
    1
  );
INSERT INTO Products (id, name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    3,
    'Lenovo IdeaPad Z575A',
    'Your Business Partner. Optimized for Windows® 7, these notebooks are ideal for SMBs. They offer multimedia tools, easy-to-use security and a sleek, vertical brushed aluminum casing. Options include discrete graphics and battery-saving mode, and two display sizes.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/fecd389225fa81a5851faf84f87eb66a38876cc2/images/copy_lenovo_59-311928_4fa79197449ff_6440116.jpg',
    0,
    459,
    1
  );
INSERT INTO Products (id, name, description, imageUrl, countAvailable, price, categoryId)
  VALUES (
    4,
    'LenovoX IdeaPad Z575A',
    'Your Business Partner. Optimized for Windows® 7, these notebooks are ideal for SMBs. They offer multimedia tools, easy-to-use security and a sleek, vertical brushed aluminum casing. Options include discrete graphics and battery-saving mode, and two display sizes.',
    'https://bitbucket.org/afrunt/exadel-spring-integration-example/raw/fecd389225fa81a5851faf84f87eb66a38876cc2/images/copy_lenovo_59-311928_4fa79197449ff_6440116.jpg',
    0,
    459,
    1
  );

INSERT INTO Orders (id, orderDate, orderStatus, userId, billingAddressId, shippingAddressId, total,phoneNumber)
  VALUES (777, NOW(), 1, 1, 1, 1, 5590,'01234567899');

INSERT INTO OrderLines (id, orderId, productId, quantity)
  VALUES (1, 777, 1, 10);