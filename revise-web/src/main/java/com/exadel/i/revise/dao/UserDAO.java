package com.exadel.i.revise.dao;

import com.exadel.i.revise.domain.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Andrey Frunt
 */
@Repository
public class UserDAO {
    @Resource
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    public List<User> findAll() {
        Session session = sessionFactory.openSession();
        return session.createCriteria(User.class).list();
    }

}
