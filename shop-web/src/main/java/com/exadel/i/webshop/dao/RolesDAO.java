package com.exadel.i.webshop.dao;

import com.exadel.i.webshop.domain.Role;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Andrey Frunt
 */
@Repository
public class RolesDAO {
    private static final RoleMapper mapper = new RoleMapper();
    @Resource
    private JdbcTemplate template;

    public List<Role> findAll() {
        return template.query("SELECT * FROM Roles", mapper);
    }

    public List<Role> findUserRoles(Long userId) {
        return template.query("SELECT r.id, r.name FROM Users u, Roles r JOIN UserRoles ur ON (ur.userId=u.id AND ur.roleId=r.id) WHERE u.id=?", new Object[]{userId}, mapper);
    }

    private static final class RoleMapper implements RowMapper<Role> {
        @Override
        public Role mapRow(ResultSet rs, int i) throws SQLException {
            Role role = new Role();
            role.setId(rs.getLong("id"));
            role.setName(rs.getString("name"));
            return role;
        }
    }
}
