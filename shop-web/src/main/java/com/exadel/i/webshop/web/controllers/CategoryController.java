package com.exadel.i.webshop.web.controllers;

import com.exadel.i.webshop.dao.CategoryDAO;
import com.exadel.i.webshop.dao.ProductDAO;
import com.exadel.i.webshop.domain.Category;
import com.exadel.i.webshop.domain.Product;
import com.exadel.i.webshop.service.ShoppingCartService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author Andrey Frunt
 */
@Controller
@RequestMapping("/")
public class CategoryController {
    @Resource
    private CategoryDAO categoryDAO;
    @Resource
    private ProductDAO productDAO;
    @Resource
    private ShoppingCartService shoppingCartService;

    @RequestMapping(value = {"/", ""}, method = GET)
    public String index(Model uiModel, HttpSession session) {
        Category firstCategory = categoryDAO.findFirst();
        uiModel.addAttribute("categories", categoryDAO.findAll());
        if (firstCategory != null) {
            return show(firstCategory.getId(), uiModel, session);
        }
        return "index";
    }

    @RequestMapping(value = {"/category/{id}/{page}", "/category/{id}/{page}/"}, method = GET)
    public String show(@PathVariable Long id, @PathVariable Integer page, Model uiModel, HttpSession session) {
        Category category = categoryDAO.findById(id);
        if (category != null) {
            uiModel.addAttribute("cart", shoppingCartService.getCart(session));
            uiModel.addAttribute("title", category.getName());
            List<Product> products = productDAO.findByCategoryId(id, page);
            uiModel.addAttribute("products", products);
            uiModel.addAttribute("categories", categoryDAO.findAll());
            uiModel.addAttribute("currentCategoryId", category.getId());
            uiModel.addAttribute("page", page);
            Integer pageCount = productDAO.calculatePageCount(id);
            uiModel.addAttribute("pageCount", pageCount);
        }
        return "category-show";
    }

    @RequestMapping(value = {"/category/{id}", "/category/{id}/"}, method = GET)
    public String show(@PathVariable Long id, Model uiModel, HttpSession session) {
        return show(id, 0, uiModel, session);
    }
}
