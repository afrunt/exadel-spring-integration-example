package com.exadel.i.webshop.test.dao;

import com.exadel.i.webshop.dao.CategoryDAO;
import com.exadel.i.webshop.domain.Category;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

import static junit.framework.Assert.*;

/**
 * @author Andrey Frunt
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app-config.xml","classpath:integration-config.xml"})
public class CategoryDAOTest {
    @Resource
    private CategoryDAO categoryDao;

    @Test
    public void testFindFirst() {
        Category category = categoryDao.findFirst();
        assertNotNull(category);
        assertEquals(Long.valueOf(1L), category.getId());
        assertEquals("Laptops",category.getName());
    }

    @Test
    public void testFindAll() {
        List<Category> all = categoryDao.findAll();
        assertFalse(all.isEmpty());
        assertEquals(3, all.size());
    }

    @Test
    public void testFindById() {
        Category category = categoryDao.findById(1L);
        assertNotNull(category);
        assertEquals(Long.valueOf(1L) , category.getId());
        assertEquals("Laptops", category.getName());
    }
}
