package com.exadel.i.webshop.service;

import com.exadel.i.webshop.dao.ProductDAO;
import com.exadel.i.webshop.domain.CartItem;
import com.exadel.i.webshop.domain.Product;
import com.exadel.i.webshop.domain.ShoppingCart;
import com.exadel.i.webshop.service.exception.NotEnoughProductsException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Frunt
 */
@Service
public class ShoppingCartService {
    private static final Log log = LogFactory.getLog(ShoppingCartService.class);
    public static final String NOT_ENOUGH_PRODUCTS = "Not enough products available to complete your order. We have only {0} units of {1}";
    @Resource
    private ProductDAO productDAO;

    /**
     * Received shoping cart from session
     *
     * @param session - current session
     * @return
     */
    public ShoppingCart getCart(HttpSession session) {
        ShoppingCart cart = (ShoppingCart) session.getAttribute("cart");
        if (cart == null) {
            cart = new ShoppingCart();
            session.setAttribute("cart", cart);
        }
        return cart;
    }

    /**
     * Refresh shopping cart
     *
     * @param session - current session
     * @return
     */
    public ShoppingCart refresh(HttpSession session) {
        log.info("ShoppingCart refreshing.");
        ShoppingCart shoppingCart = getCart(session);
        List<CartItem> items = shoppingCart.getItems();
        Map<Long, Product> productMap = ProductDAO.asMap(productDAO.findByIds(shoppingCart.getProductIds()));

        for (CartItem item : items) {
            item.setPrice(productMap.get(item.getProductId()).getPrice());
        }
        log.info("ShoppingCart refreshed.");
        return shoppingCart;
    }

    public void addToCart(ShoppingCart cart, long productId) throws NotEnoughProductsException {
        CartItem item = findItemByProductId(cart, productId);
        Product product = productDAO.findById(productId);
        log.info("Adding product to ShoppingCart.CategotyId="+ product.getCategoryId() + ".ProductId=" + productId);

        int neededQuantity = (item != null) ? item.getQuantity() + 1 : 1;

        if(neededQuantity > product.getCountAvailable()) {
            String message = MessageFormat.format(NOT_ENOUGH_PRODUCTS,product.getCountAvailable(),neededQuantity);
            log.error(message);
            throw new NotEnoughProductsException(product.getCountAvailable(), product.getName());
        }

        if (item == null) {
            item = new CartItem();
            item.setQuantity(1);
            item.setPrice(product.getPrice());
            item.setProductId(productId);
            cart.addItem(item);
            log.info("New item added to ShoppingCart");
        } else {
            item.setQuantity(neededQuantity);
            log.info("Item updated in ShoppingCart.Quantity=" + item.getQuantity());
        }

        cart.setTotal(calculateCartTotal(cart));
        cart.setTotalProductCount(calculateTotalProductCount(cart));
    }

    /**
     * Remove product from shopping cart
     *
     * @param cart      - shopping cart
     * @param productId - product id
     */
    public void removeFromCart(ShoppingCart cart, long productId) {
        log.info("Removing product from ShoppingCart.ProductId=" + productId);
        CartItem item = findItemByProductId(cart, productId);
        if (item != null) {
            cart.removeItem(item);
            cart.setTotal(calculateCartTotal(cart));
            cart.setTotalProductCount(calculateTotalProductCount(cart));
            log.info("Product removed.");
        }else{
            log.info("Product not found.");
        }
    }

    public void clear(ShoppingCart cart) {
        cart.clear();
    }

    private int calculateTotalProductCount(ShoppingCart cart) {
        int total = 0;
        for (CartItem item : cart.getItems()) {
            total += item.getQuantity();
        }
        return total;
    }

    private double calculateCartTotal(ShoppingCart cart) {
        double total = 0;
        for (CartItem item : cart.getItems()) {
            total += item.getPrice() * item.getQuantity();
        }
        return total;
    }

    private CartItem findItemByProductId(ShoppingCart cart, long productId) {
        List<CartItem> items = cart.getItems();
        for (CartItem ci : items) {
            if (ci.getProductId() == productId) {
                return ci;
            }
        }
        return null;
    }
}
