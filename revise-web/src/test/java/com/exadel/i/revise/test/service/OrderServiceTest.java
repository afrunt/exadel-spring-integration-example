package com.exadel.i.revise.test.service;

import com.exadel.i.revise.dao.OrderDAO;
import com.exadel.i.revise.domain.OrderMessage;
import com.exadel.i.revise.domain.Order;
import com.exadel.i.revise.domain.OrderLine;
import com.exadel.i.revise.domain.Product;
import com.exadel.i.revise.service.OrderService;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * @author Ksenia Orlenko
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app-config.xml", "classpath:integration-config.xml"})
public class OrderServiceTest {

    @Resource
    private OrderService orderService;
    @Resource
    private OrderDAO orderDAO;
    private final Order order = new Order();
    private final OrderLine orderLine = new OrderLine();
    private final OrderMessage orderMessage = new OrderMessage();
    private final OrderMessage.MessageOrderLine messageOrderLine = new OrderMessage.MessageOrderLine();

    @Before
    public void init() {
        order.setId(1L);
        order.setBillingAddress("Billing Address");
        order.setShippingAddress("Shipping address");
        order.setTotal(500.00);
        order.setPhoneNumber("01234567899");
        orderLine.setId(1L);
        orderLine.setProductId(1L);
        orderLine.setImageUrl("image url");
        orderLine.setName("Order line name");
        orderLine.setPrice(500.00);
        orderLine.setQuantity(1);
        orderLine.setOrder(order);
        orderMessage.setBillingAddress("Billing Address");
        orderMessage.setShippingAddress("Shipping address");
        orderMessage.setId(1L);
        orderMessage.setTotal(500.00);
        orderMessage.setPhoneNumber("1111");
        messageOrderLine.setId(1L);
        messageOrderLine.setProductId(1L);
        messageOrderLine.setImageUrl("url");
        messageOrderLine.setName("name");
        messageOrderLine.setPrice(500.00);
        messageOrderLine.setQuantity(1);
        orderMessage.setOrderLines(Arrays.asList(messageOrderLine));
    }

    @Test
    public void testFindAll() throws Exception {
        orderDAO.saveOrder(order);
        List<Order> all = orderService.findAll();
        Assert.assertNotNull("Order must be founded", all);
        Assert.assertEquals("Order list size must be 1", 1, all.size());
        orderDAO.delete(order);
    }

    @Test
    public void testFindById() throws Exception {
        orderDAO.saveOrder(order);
        Order orderFounded = orderService.findById(order.getId());
        Assert.assertNotNull("Order must be founded", orderFounded);
        Assert.assertEquals("Order id must be 1", order.getId(), orderFounded.getId());
        orderDAO.delete(order);
    }

    @Test
    public void testReceiveNewOrder() throws Exception {
        orderService.receiveNewOrder(orderMessage);
        Order orderSaved = orderDAO.findById(orderMessage.getId());
        Assert.assertNotNull("Order must be founded", orderSaved);
        Assert.assertEquals("Order id must be 1", orderMessage.getId(), orderSaved.getId());
        Assert.assertNotNull("OrderLine must be not null", orderSaved.getOrderLines());
        Assert.assertEquals("OrderLine list size must be 1", 1, orderSaved.getOrderLines().size());
        orderDAO.delete(orderSaved);
    }

    @Test
    public void testReviseOrder() throws Exception {
        orderDAO.saveOrder(order);
        orderService.reviseOrder(order.getId());
        Order orderNotFounded = orderDAO.findById(order.getId());
        Assert.assertNull("Order must be not founded", orderNotFounded);
    }

    @Test
    public void testRejectOrder() throws Exception {
        orderDAO.saveOrder(order);
        orderService.rejectOrder(order.getId());
        Order orderNotFounded = orderDAO.findById(order.getId());
        Assert.assertNull("Order must be not founded", orderNotFounded);
    }
}
