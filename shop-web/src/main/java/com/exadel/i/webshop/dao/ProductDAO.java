package com.exadel.i.webshop.dao;

import com.exadel.i.webshop.domain.Product;
import com.exadel.i.webshop.util.PagerUtil;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Frunt
 */
@Repository
public class ProductDAO {
    private static final ProductMapper mapper = new ProductMapper();

    @Resource
    private JdbcTemplate template;

    public static Map<Long, Product> asMap(List<Product> products) {
        Map<Long, Product> map = new HashMap<Long, Product>();
        for (Product product : products) {
            map.put(product.getId(), product);
        }
        return map;
    }

    public Product findById(Long productId) {
        return template.queryForObject("SELECT * FROM Products WHERE id=?", new Object[]{productId}, mapper);
    }

    public List<Product> findByCategoryId(Long categoryId) {
        return template.query("SELECT * FROM Products WHERE categoryId=?", new Object[]{categoryId}, mapper);
    }

    public List<Product> findByIds(List<Long> ids) {
        if (ids == null || ids.isEmpty()) {
            return Collections.emptyList();
        } else {
            StringBuilder queryBuilder = new StringBuilder("SELECT * FROM Products WHERE id IN (");
            boolean firstId = true;
            for (Long id : ids) {
                if (!firstId) {
                    queryBuilder.append(",");
                }
                if (firstId) {
                    firstId = false;
                }
                queryBuilder.append(id);
            }
            queryBuilder.append(")");
            return template.query(queryBuilder.toString(), mapper);
        }
    }


    public void decreaseAvailableCount(Long id, Integer quantity) {
        template.update("UPDATE Products SET countAvailable=countAvailable-? WHERE id= ?", new Object[]{quantity, id});
    }

    public void increaseAvailableCount(Long id, Integer quantity) {
        template.update("UPDATE Products SET countAvailable=countAvailable+? WHERE id= ?", new Object[]{quantity, id});
    }

    public List<Product> findByCategoryId(Long categoryId, int page, int perPage) {
        int totalCount = template.queryForInt("SELECT count(*) FROM Products WHERE categoryId=?", new Object[]{categoryId});
        int pageCount = PagerUtil.calculatePageCount(totalCount, perPage);
        int offset = PagerUtil.calculateOffset(totalCount, page, pageCount);

        return template.query("SELECT * FROM Products WHERE categoryId=? ORDER BY name LIMIT ? OFFSET ?",
                new Object[]{categoryId, perPage, offset}, mapper);
    }

    public List<Product> findByCategoryId(Long categoryId, int page) {
        return findByCategoryId(categoryId, page, PagerUtil.DEFAULT_PER_PAGE);
    }

    public Integer calculatePageCount(Long categoryId) {
        int totalCount = template.queryForInt("SELECT count(*) FROM Products WHERE categoryId=?", new Object[]{categoryId});
        return PagerUtil.calculatePageCount(totalCount, PagerUtil.DEFAULT_PER_PAGE);
    }

    public List<Product> findByKeyword(String keyword) {
        return template.query("SELECT * FROM PRODUCTS WHERE LCASE(Name) LIKE CONCAT('%',?,'%') ORDER BY categoryId",
                new Object[]{keyword.toLowerCase()}, mapper);
    }

    private static class ProductMapper implements RowMapper<Product> {
        @Override
        public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
            Product p = new Product();
            p.setId(rs.getLong("id"));
            p.setName(rs.getString("name"));
            p.setCategoryId(rs.getLong("categoryId"));
            p.setCountAvailable(rs.getLong("countAvailable"));
            p.setDescription(rs.getString("description"));
            p.setImageUrl(rs.getString("imageUrl"));
            p.setPrice(rs.getDouble("price"));
            return p;
        }
    }

}
