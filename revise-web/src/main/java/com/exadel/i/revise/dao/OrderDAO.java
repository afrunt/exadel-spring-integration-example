package com.exadel.i.revise.dao;

import com.exadel.i.revise.domain.Order;
import com.exadel.i.revise.domain.OrderLine;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Andrey Frunt
 */
@Repository
@Transactional
public class OrderDAO {
    @Resource
    private SessionFactory sessionFactory;

    public Order findById(Long id) {
        return (Order) sessionFactory.getCurrentSession().createCriteria(Order.class).add(Restrictions.eq("id", id)).uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public List<Order> findAll() {
        return sessionFactory.getCurrentSession().createCriteria(Order.class).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();
    }

    public void saveOrder(Order order) {
        sessionFactory.getCurrentSession().save(order);
    }

    public void saveOrderLine(OrderLine orderLine) {
        sessionFactory.getCurrentSession().save(orderLine);
    }

    public void delete(Order order) {
        sessionFactory.getCurrentSession().delete(order);
    }
}
