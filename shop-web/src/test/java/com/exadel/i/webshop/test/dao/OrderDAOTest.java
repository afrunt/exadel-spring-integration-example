package com.exadel.i.webshop.test.dao;

import com.exadel.i.webshop.dao.OrderDAO;
import com.exadel.i.webshop.domain.Order;
import com.exadel.i.webshop.domain.OrderLine;
import com.exadel.i.webshop.domain.OrderStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.*;

/**
 * @author Ksenia Orlenko
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app-config.xml","classpath:integration-config.xml"})
public class OrderDAOTest {
    @Resource
    private OrderDAO orderDAO;
    private final Order order = new Order();
    private final OrderLine orderLine = new OrderLine();

    @Before
    public void init() {
        orderLine.setOrderId(777L);
        orderLine.setProductId(2L);
        orderLine.setQuantity(15);
        order.setOrderDate(new Date());
        order.setOrderStatus(OrderStatus.NEW);
        order.setUserId(1L);
        order.setTotal(500.00);
        order.setBillingAddressId(1L);
        order.setShippingAddressId(1L);
        order.setPhoneNumber("01234567899");
    }

    @Test
    public void testFindById() {
        Order order = orderDAO.findById(777L);
        assertNotNull("Order must be found", order);
        assertEquals("Order status must be 2", OrderStatus.NEW, order.getOrderStatus());
        assertEquals("User id must be 1", Long.valueOf(1L), order.getUserId());
        assertEquals("Order id must be 777L", Long.valueOf(777L), order.getId());
        assertEquals("Order phone must be 01234567899", "01234567899", order.getPhoneNumber());
    }

    @Test
    public void testFindAll() {
        List<Order> all = orderDAO.findAll();
        assertFalse("List must not be empty", all.isEmpty());
        assertEquals("One order in DB", 1, all.size());
    }

    @Test
    public void testFindByProductId() {
        OrderLine orderLine = orderDAO.findByProductId(1L);
        assertNotNull("Order Line must be found", orderLine);
        assertEquals("Order Line quantity must be 10", Integer.valueOf(10), orderLine.getQuantity());
        assertEquals("Product id must be 1", Long.valueOf(1L), orderLine.getProductId());
    }

    @Test
    public void testSave() {
        Long id = orderDAO.create(order);
        Order orderFound = orderDAO.findById(id);
        assertNotNull(orderFound);
        orderDAO.deleteOrder(id);
    }

    @Test
    public void testUpdateStatus() throws Exception {
        Long id = orderDAO.create(order);
        Order order = orderDAO.findById(id);
        assertEquals(OrderStatus.NEW, order.getOrderStatus());
        orderDAO.updateOrderStatus(id, OrderStatus.REVISING);
        Order actualOrder = orderDAO.findById(id);
        assertNotNull(actualOrder);
        assertEquals(OrderStatus.REVISING, actualOrder.getOrderStatus());
        orderDAO.deleteOrder(id);
    }

    @Test
    public void testSaveOrderLine() {
        orderDAO.createOrderLine(orderLine);
        OrderLine line = orderDAO.findByProductId(2L);
        assertNotNull("OrderLine must be found", line);
        assertEquals("OrderLine quantity must be 15", Integer.valueOf(15), line.getQuantity());
        assertEquals("Product id must be 2", Long.valueOf(2L), line.getProductId());
        orderDAO.deleteOrderLine(line.getId());
    }

    @Test
    public void testFindOrderWithOrderLines() {
        Order order = orderDAO.findByIdWithOrderLines(777L);
        assertNotNull("Order must be found", order);
        assertNotNull("OrderLine must be found", order.getOrderLines());
        assertEquals("Must fount one order line", 1, order.getOrderLines().size());
    }

    @Test
    public void testUpdateOrderLineQuantity() {
        orderDAO.createOrderLine(orderLine);
        OrderLine line = orderDAO.findByProductId(2L);
        assertEquals("Quantity must by 15", Integer.valueOf(15), line.getQuantity());
        orderDAO.increaseOrderLineQuantity(line.getId());
        OrderLine refreshLine = orderDAO.findByProductId(2L);
        assertEquals("Quantity must be 16", Integer.valueOf(16), refreshLine.getQuantity());
        orderDAO.decreaseOrderLineQuantity(refreshLine.getId());
        OrderLine orderLine = orderDAO.findByProductId(2L);
        assertEquals("Quantity must by 15", Integer.valueOf(15), orderLine.getQuantity());
        orderDAO.deleteOrderLine(refreshLine.getId());
    }

    @Test
    public void testUpdateOrderTotal() {
        Long id = orderDAO.create(order);
        orderDAO.updateOrderTotal(id, 100.00);
        Order refresh = orderDAO.findById(id);
        assertEquals("Total must be:", 100.00, refresh.getTotal());
        orderDAO.deleteOrder(id);
    }

    @Test
    public void testFindByUserId() throws Exception {
        List<Order> orders = orderDAO.findByUserId(1L);
        assertNotNull("Order must be found", orders);
        assertEquals("Must found one order", 1, orders.size());
        assertEquals("Order id must be 777", Long.valueOf(777L), orders.get(0).getId());
    }
}
