package com.exadel.i.webshop.test.service;

import com.exadel.i.webshop.domain.CartItem;
import com.exadel.i.webshop.domain.ShoppingCart;
import com.exadel.i.webshop.service.ShoppingCartService;
import com.exadel.i.webshop.service.exception.NotEnoughProductsException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app-config.xml","classpath:integration-config.xml"})
public class ShoppingCartServiceTest {
    @Resource
    private ShoppingCartService shoppingCartService;

    private final ShoppingCart shoppingCart = new ShoppingCart();

    @Before
    public void init() {
        CartItem cartItem = new CartItem();
        cartItem.setPrice(159);
        cartItem.setProductId(1L);
        cartItem.setQuantity(1);
        shoppingCart.setTotalProductCount(1);
        shoppingCart.addItem(cartItem);
        shoppingCart.setTotal(1);
    }

    @Test
    public void testGetCartIfNotExist() {
        HttpSession httpSession = mock(HttpSession.class);
        ShoppingCart cart = shoppingCartService.getCart(httpSession);
        assertNotNull(cart);
    }

    @Test
    public void testGetCartIfExist() {
        HttpSession httpSession = mock(HttpSession.class);
        when(httpSession.getAttribute("cart")).thenReturn(shoppingCart);
        ShoppingCart cart = shoppingCartService.getCart(httpSession);
        assertNotNull(cart);
        assertEquals("Cart must contain 1 item", 1, cart.getItems().size());
    }

    @Test
    public void testRefreshModifiedPrice() {
        HttpSession httpSession = mock(HttpSession.class);
        when(httpSession.getAttribute("cart")).thenReturn(shoppingCart);
        assertEquals("Price must be 559", Double.valueOf(159), Double.valueOf(shoppingCart.getItems().get(0).getPrice()));
        ShoppingCart cart = shoppingCartService.refresh(httpSession);
        assertNotNull(cart);
        assertEquals("Price must be 559", Double.valueOf(559), Double.valueOf(cart.getItems().get(0).getPrice()));
    }

    @Test
    public void testAddToCartIfProductExistInCart() {
        try {
            shoppingCartService.addToCart(shoppingCart, 1L);
            assertEquals("Product id must by 1", 1L, shoppingCart.getItems().get(0).getProductId());
            assertEquals("Quantity of product must by 2", 2, shoppingCart.getItems().get(0).getQuantity());
        } catch (NotEnoughProductsException e) {
            fail("NotEnoughProductsException" + e.getMessage());
        }
    }

    @Test
    public void testAddToCartIfProductNotExistInCart() {
        try {
            shoppingCartService.addToCart(shoppingCart, 2L);
            assertEquals("Item size must be 2", 2, shoppingCart.getItems().size());
        } catch (NotEnoughProductsException e) {
            fail("NotEnoughProductsException" + e.getMessage());
        }
    }

    @Test(expected = NotEnoughProductsException.class)
    public void testAddToCartIfNotEnoughProducts() throws NotEnoughProductsException {
        shoppingCart.getItems().get(0).setQuantity(100);
        shoppingCartService.addToCart(shoppingCart, 1L);
    }

    @Test
    public void testRemoveFromCart() {
        try {
            shoppingCartService.addToCart(shoppingCart, 2L);
        } catch (NotEnoughProductsException e) {
            fail("NotEnoughProductsException" + e.getMessage());
        }
        assertEquals("Item size must be 2", 2, shoppingCart.getItems().size());
        shoppingCartService.removeFromCart(shoppingCart, 2L);
        assertEquals("Item size must be 1", 1, shoppingCart.getItems().size());
    }

    @Test
    public void testClear() {
        shoppingCartService.clear(shoppingCart);
        assertTrue(0 == shoppingCart.getTotal());
        assertEquals(0, shoppingCart.getTotalProductCount());
        assertEquals(0, shoppingCart.getItems().size());
    }

    @Test
    public void testContainsProduct() {
        assertTrue(shoppingCart.containsProduct(1L));
        assertFalse(shoppingCart.containsProduct(12093L));
    }
}
