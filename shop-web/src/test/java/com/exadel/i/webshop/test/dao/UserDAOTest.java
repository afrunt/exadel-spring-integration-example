package com.exadel.i.webshop.test.dao;

import com.exadel.i.webshop.dao.UserDAO;
import com.exadel.i.webshop.domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

import static junit.framework.Assert.*;

/**
 * @author Andrey Frunt
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app-config.xml", "classpath:integration-config.xml"})
public class UserDAOTest {
    @Resource
    private UserDAO userDAO;

    @Test
    public void testFindById() {
        User user = userDAO.findById(1L);
        assertNotNull("User must be found", user);
        assertEquals("User id must be", Long.valueOf(1L), user.getId());
        assertEquals("User must be John Doe", "John Doe", user.getFirstName() + " " + user.getLastName());
        assertEquals("john.doe@gmail.com", user.getEmail());
        assertNotNull(user.getPasswordHash());
        assertEquals("01234567899", user.getPhoneNumber());
    }

    @Test
    public void testFindAll() {
        List<User> all = userDAO.findAll();
        assertFalse("List must not be empty", all.isEmpty());
        assertEquals("One user in DB", 3, all.size());
    }

    @Test
    public void testFindByEmail() {
        User user = userDAO.findByEmail("john.doe@gmail.com");
        assertNotNull("User must be found", user);
        assertEquals("User id must be", Long.valueOf(1L), user.getId());
        assertEquals("User must be John Doe", "John Doe", user.getFirstName() + " " + user.getLastName());
        assertEquals("john.doe@gmail.com", user.getEmail());
        assertNotNull(user.getPasswordHash());
        assertEquals("01234567899", user.getPhoneNumber());
    }
}
