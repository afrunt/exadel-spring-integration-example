package com.exadel.i.webshop.domain;

/**
 * @author Andrey Frunt
 */
public class BillingAddress extends AbstractAddress {

    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return getAddress() + "," + getCity() + "," + getState();
    }
}
