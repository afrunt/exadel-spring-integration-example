package com.exadel.i.webshop.ws;

import com.exadel.i.webshop.dao.BillingAddressDAO;
import com.exadel.i.webshop.dao.OrderDAO;
import com.exadel.i.webshop.dao.ProductDAO;
import com.exadel.i.webshop.dao.ShippingAddressDAO;
import com.exadel.i.webshop.domain.*;
import com.exadel.i.webshop.service.OrderService;
import com.exadel.i.webshop.service.exception.NotEnoughProductsException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Andrey Frunt
 */
@Service
public class RemoteService {

    private static final Log log = LogFactory.getLog(RemoteService.class);

    public static final String NOT_ENOUGH_PRODUCTS = "Not enough products available to complete your order. We have only {0} units of {1}";
    @Resource
    private ProductDAO productDAO;
    @Resource
    private OrderService orderService;
    @Resource
    private OrderDAO orderDAO;
    @Resource
    private BillingAddressDAO billingAddressDAO;
    @Resource
    private ShippingAddressDAO shippingAddressDAO;
    @Resource
    private MessageSource messageSourceDefault;

    /**
     * Received soap request form revise module and search product by keyword
     *
     * @param request - object request that contain keyword
     * @return - object response that contains search result
     */
    public SearchResponse search(SearchRequest request) {
        log.info("Received soap request with keyword: " + request.getSearchTerm());
        SearchResponse searchResponse = new SearchResponse();
        List<Product> products = productDAO.findByKeyword(request.getSearchTerm());
        log.info("Create search response");
        for (Product product : products) {
            SearchResponse.SearchResponseItem item = new SearchResponse.SearchResponseItem();
            item.setName(product.getName());
            item.setCountAvailable(product.getCountAvailable());
            item.setProductId(product.getId());
            item.setImageUrl(product.getImageUrl());
            item.setPrice(product.getPrice());
            searchResponse.getItems().add(item);
        }
        log.info("Send search response to revise");
        return searchResponse;
    }

    /**
     * Received soap request form revise module and remove product from order
     *
     * @param request object request that contain product and order id
     * @return object response that contains updated order or error if cant update them
     */
    public AddRemoveResponse remove(AddRemoveRequest request) {
        log.info("Received soap request to delete product from order with order id:" + request.getOrderId() + " and product id:" + request.getProductId());
        AddRemoveResponse response = new AddRemoveResponse();
        try {
            Order order = orderDAO.findById(request.getOrderId());
            if (order == null) {
                String message = "Order not found";
                log.error(message);
                throw new Exception(message);
            } else {
                orderService.removeProductFromOrder(request.getOrderId(), request.getProductId());
                log.info("Remove product with id: " + request.getProductId() + " from order with id:" + request.getOrderId());
                Order updatedOrder = orderDAO.findByIdWithOrderLines(request.getOrderId());
                if (updatedOrder.getOrderLines().isEmpty()) {
                    orderDAO.deleteOrder(updatedOrder.getId());
                    log.info("Delete order with id:" + updatedOrder.getId());
                    response.setSuccess(true);
                } else {
                    ResponseOrder responseOrder = convertOrderToResponseOrder(updatedOrder);
                    response.setResponseOrder(responseOrder);
                    log.info("Create new response order with id:" + responseOrder.getId());
                    response.setSuccess(true);
                }
            }
        } catch (Exception e) {
            response.setSuccess(false);
            String message = e.getMessage();
            log.error(message);
            response.setErrorMessage(message);
        }
        log.info("Send remove response to revise");
        return response;
    }

    /**
     * Received soap request form revise module and add product to order
     *
     * @param request object request that contain product and order id
     * @return object response that contains updated order or error if cant update them
     */
    public AddRemoveResponse add(AddRemoveRequest request) {
        log.info("Received soap request to add product from order with order id:" + request.getOrderId() + " and product id:" + request.getProductId());
        AddRemoveResponse response = new AddRemoveResponse();
        try {
            Order order = orderDAO.findById(request.getOrderId());
            if (order == null) {
                String message = "Order not found";
                log.error(message);
                throw new Exception(message);
            } else {
                orderService.addProductToOrder(request.getOrderId(), request.getProductId());
                log.info("Add product with id: " + request.getProductId() + " from order with id:" + request.getOrderId());
                Order orderWithOrderLine = orderDAO.findByIdWithOrderLines(request.getOrderId());
                ResponseOrder responseOrder = convertOrderToResponseOrder(orderWithOrderLine);
                log.info("Create new response order with id:" + responseOrder.getId());
                response.setSuccess(true);
                response.setResponseOrder(responseOrder);
            }
        } catch (NotEnoughProductsException e) {
            String message = MessageFormat.format(NOT_ENOUGH_PRODUCTS, e.getCountAvailable(), e.getProductName());
            log.error(message);
            response.setSuccess(false);
            response.setErrorMessage(message);
        } catch (Exception e) {
            response.setSuccess(false);
            String message = e.getMessage();
            log.error(message);
            response.setErrorMessage(message);
        }
        log.info("Send add response to revise");
        return response;
    }

    /**
     * Convert Order to ResponseOrder
     *
     * @param order - order
     * @return ResponseOrder
     */
    private ResponseOrder convertOrderToResponseOrder(Order order) {
        log.info("Converting order to order response");
        ResponseOrder responseOrder = new ResponseOrder();
        responseOrder.setId(order.getId());
        BillingAddress billingAddress = billingAddressDAO.findById(order.getBillingAddressId());
        responseOrder.setBillingAddress(billingAddress.toString());
        ShippingAddress shippingAddress = shippingAddressDAO.findById(order.getShippingAddressId());
        responseOrder.setShippingAddress(shippingAddress.toString());
        responseOrder.setPhoneNumber(order.getPhoneNumber());
        responseOrder.setTotal(order.getTotal());
        List<OrderLine> orderLines = order.getOrderLines();
        List<Long> productIds = new ArrayList<Long>(orderLines.size());
        for (OrderLine orderLine : orderLines) {
            productIds.add(orderLine.getProductId());
        }
        Map<Long, Product> productsMap = ProductDAO.asMap(productDAO.findByIds(productIds));
        ArrayList<ResponseOrder.ResponseOrderLine> responseOrderLines = new ArrayList<ResponseOrder.ResponseOrderLine>();
        for (OrderLine orderLine : orderLines) {
            ResponseOrder.ResponseOrderLine responseOrderLine = new ResponseOrder.ResponseOrderLine();
            responseOrderLine.setId(orderLine.getId());
            responseOrderLine.setProductId(orderLine.getProductId());
            responseOrderLine.setQuantity(orderLine.getQuantity());
            Product product = productsMap.get(orderLine.getProductId());
            responseOrderLine.setName(product.getName());
            responseOrderLine.setImageUrl(product.getImageUrl());
            responseOrderLine.setPrice(product.getPrice());
            responseOrderLines.add(responseOrderLine);
        }
        responseOrder.setOrderLines(responseOrderLines);
        log.info("Converted order to order response");
        return responseOrder;
    }
}
