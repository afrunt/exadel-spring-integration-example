package com.exadel.i.revise.test.dao;

import com.exadel.i.revise.dao.UserDAO;
import com.exadel.i.revise.domain.User;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Andrey Frunt
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app-config.xml","classpath:integration-config.xml"})
public class UserDAOTest {
    @Resource
    private UserDAO dao;

    @Test
    public void findAll() {
        List<User> all = dao.findAll();
        Assert.assertNotNull(all);
    }

}
