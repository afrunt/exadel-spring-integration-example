package com.exadel.i.packaging.dao;

import com.exadel.i.packaging.domain.Order;
import com.exadel.i.packaging.domain.OrderLine;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * @author Ksenia Orlenko
 */
@Repository
@Transactional
public class OrderDAO {
    @PersistenceContext
    private EntityManager em;


    @Transactional(readOnly = true)
    public Order findById(Long id) {
        return em.find(Order.class, id);
    }

    @Transactional(readOnly = true)
    public List<Order> findAll() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Order> criteriaQuery = criteriaBuilder.createQuery(Order.class);
        Root<Order> orderRoot = criteriaQuery.from(Order.class);
        criteriaQuery.select(orderRoot);
        return em.createQuery(criteriaQuery).getResultList();
    }

    public void create(Order order) {
        em.persist(order);
    }

    public void delete(Order order) {
        if (!em.contains(order)) {
            order = em.merge(order);
        }
        em.remove(order);
    }

    public void deleteById(Long id) {
        if (id != null) {
            Query query = em.createQuery("DELETE FROM Order o WHERE o.id=:id").setParameter("id", id);
            query.executeUpdate();
        }
    }

    public void createOrderLine(OrderLine orderLine) {
        em.persist(orderLine);
    }
}
