package com.exadel.i.webshop.test.dao;

import com.exadel.i.webshop.dao.ProductDAO;
import com.exadel.i.webshop.domain.Product;
import com.exadel.i.webshop.util.PagerUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static junit.framework.Assert.*;

/**
 * @author Andrey Frunt
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app-config.xml","classpath:integration-config.xml"})
public class ProductDAOTest {
    @Resource
    private ProductDAO productDao;

    @Test
    public void testFindById() {
        Product product = productDao.findById(1L);
        assertNotNull(product);
        assertEquals("Fujitsu Lifebook AH512", product.getName());
        assertEquals("Product id must be 1", Long.valueOf(1L), product.getId());
        assertEquals("Category must be 1", Long.valueOf(1L), product.getCategoryId());
        assertEquals("Count of product must be 50", Long.valueOf(50L), product.getCountAvailable());
        assertNotNull(product.getImageUrl());
        assertNotNull(product.getDescription());
    }

    @Test
    public void testFindByIds() {
        List<Product> productList = productDao.findByIds(Arrays.asList(1L, 2L));
        assertFalse("List must not be empty", productList.isEmpty());
    }

    @Test
    public void testFindByIdsWhenEmpty() throws Exception {
        List<Product> products = productDao.findByIds(Collections.<Long>emptyList());
        assertTrue("List of product must be empty", products.isEmpty());
    }

    @Test
    public void testFindByCategoryIdPaged() {
        List<Product> products = productDao.findByCategoryId(1L);
        assertFalse("List must not be empty", products.isEmpty());
        assertEquals("Must found 4 product", 4, products.size());
        products = productDao.findByCategoryId(1L, 0);
        assertFalse("List must not be empty", products.isEmpty());
        assertEquals("Must found 4 product", 4, products.size());
        products = productDao.findByCategoryId(1L, 0, PagerUtil.DEFAULT_PER_PAGE);
        assertFalse("List must not be empty", products.isEmpty());
        assertEquals("Must found 4 product", 4, products.size());
    }

    @Test
    public void testFindByKeyword() {
        List<Product> products = productDao.findByKeyword("Lenovo IdeaPad Z575A");
        assertEquals(1, products.size());
        assertEquals("Product id must by 3", Long.valueOf(3L), products.get(0).getId());
        List<Product> list = productDao.findByKeyword("Len");
        assertEquals(2, list.size());
        list = productDao.findByKeyword("lEn");
        assertEquals(2, list.size());
        list = productDao.findByKeyword("Z575");
        assertEquals(2, list.size());
        list = productDao.findByKeyword("a");
        assertEquals(4, list.size());
    }

    @Test
    public void testCalculatePageCount() {
        Integer pageCount = productDao.calculatePageCount(1L);
        assertNotNull("Page count cannot be null", pageCount);
        assertEquals("Page count must be 1", Integer.valueOf(1), pageCount);
    }

    @Test
    public void testUpdateCountAvailable() {
        Product product = productDao.findById(1L);
        productDao.decreaseAvailableCount(product.getId(), 10);
        Product refreshProduct = productDao.findById(product.getId());
        assertEquals("Product count must be:", Long.valueOf(40L), refreshProduct.getCountAvailable());
        productDao.increaseAvailableCount(product.getId(), 10);
    }
}
