package com.exadel.i.webshop.service.exception;

/**
 * @author Andrey Frunt
 */
public class NotEnoughProductsException extends AbstractServiceException {
    private long countAvailable;
    private String productName;

    public NotEnoughProductsException(long countAvailable, String productName) {
        this.countAvailable = countAvailable;
        this.productName = productName;
    }

    public long getCountAvailable() {
        return countAvailable;
    }

    public String getProductName() {
        return productName;
    }
}
