package com.exadel.i.webshop.test.dao;

import com.exadel.i.webshop.dao.ShippingAddressDAO;
import com.exadel.i.webshop.domain.BillingAddress;
import com.exadel.i.webshop.domain.ShippingAddress;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

import static junit.framework.Assert.*;
import static junit.framework.Assert.assertEquals;

/**
 * @author Andrey Frunt
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app-config.xml","classpath:integration-config.xml"})
public class ShippingAddressDAOTest {
    @Resource
    private ShippingAddressDAO shippingAddressDAO;
    private final ShippingAddress ShippingAddress = new ShippingAddress();

    @Before
    public void init() {
        ShippingAddress.setUserId(2L);
        ShippingAddress.setName("Alex");
        ShippingAddress.setState("CA");
        ShippingAddress.setCity("Walnut Creek");
        ShippingAddress.setAddress("1340 Treat Blvd.");
        ShippingAddress.setZip("90210");
    }

    @Test
    public void testFindById() throws Exception {
        ShippingAddress address = shippingAddressDAO.findById(1L);
        assertNotNull(address);
    }

    @Test
    public void testFindByUserId() {
        List<ShippingAddress> shippingAddress = shippingAddressDAO.findByUserId(1L);
        assertFalse(shippingAddress.isEmpty());
        assertEquals(1, shippingAddress.size());
        assertEquals("John Doe",shippingAddress.get(0).getName());
        assertEquals("CA", shippingAddress.get(0).getState());
        assertEquals("Walnut Creek", shippingAddress.get(0).getCity());
        assertEquals("1340 Treat Blvd.",shippingAddress.get(0).getAddress());
        assertEquals("94597",shippingAddress.get(0).getZip());
    }

    @Test
    public void testSave(){
        shippingAddressDAO.create(ShippingAddress);
        List<ShippingAddress> addresses = shippingAddressDAO.findByUserId(2L);
        assertEquals(1, addresses.size());
        assertEquals("Alex", addresses.get(0).getName());
        assertEquals("CA", addresses.get(0).getState());
        assertEquals("Walnut Creek", addresses.get(0).getCity());
        assertEquals("1340 Treat Blvd.", addresses.get(0).getAddress());
        assertEquals("90210", addresses.get(0).getZip());
        shippingAddressDAO.delete(addresses.get(0).getId());
        List<ShippingAddress> refreshAddresses = shippingAddressDAO.findByUserId(2L);
        assertTrue(refreshAddresses.isEmpty());

    }
}
