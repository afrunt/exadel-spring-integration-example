package com.exadel.i.webshop.test.service;

import com.exadel.i.webshop.dao.BillingAddressDAO;
import com.exadel.i.webshop.dao.OrderDAO;
import com.exadel.i.webshop.dao.ProductDAO;
import com.exadel.i.webshop.domain.*;
import com.exadel.i.webshop.service.OrderService;
import com.exadel.i.webshop.service.exception.NotEnoughProductsException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app-config.xml", "classpath:integration-config.xml"})
public class OrderServiceTest {
    @Resource
    OrderService orderService;
    @Resource
    OrderDAO orderDAO;
    @Resource
    ProductDAO productDAO;
    @Resource
    BillingAddressDAO billingAddressDAO;

    private final ShoppingCart shoppingCart = new ShoppingCart();
    private final Order order = new Order();
    private final OrderLine orderLine = new OrderLine();

    @Before
    public void init() {
        CartItem cartItem = new CartItem();
        cartItem.setPrice(159);
        cartItem.setProductId(1L);
        cartItem.setQuantity(1);
        shoppingCart.addItem(cartItem);
        order.setUserId(1L);
        order.setBillingAddressId(1L);
        order.setOrderDate(new Date());
        order.setOrderStatus(OrderStatus.NEW);
        order.setShippingAddressId(1L);
        order.setTotal(0.00);
        order.setPhoneNumber("01234567899");
        orderLine.setProductId(1L);
        orderLine.setQuantity(50);
    }

    @Test
    public void testSubmitOrder() {
        long id = 0;
        try {
            id = orderService.submitOrder(shoppingCart, 1L, "01234567899", 1L, 1L);
            Order order = orderDAO.findById(id);
            assertNotNull("Order must be found", order);
        } catch (NotEnoughProductsException e) {
            fail("NotEnoughProductsException" + e.getMessage());
        } finally {
            productDAO.increaseAvailableCount(1L, shoppingCart.getItems().get(0).getQuantity());
            orderDAO.deleteOrder(id);
        }
    }

    @Test(expected = NotEnoughProductsException.class)
    public void testSubmitOrderIfNotEnoughProducts() throws NotEnoughProductsException {
        CartItem cartItem = shoppingCart.getItems().get(0);
        cartItem.setQuantity(100);
        long id = orderService.submitOrder(shoppingCart, 1L, "01234567899", 1L, 1L);
        orderDAO.deleteOrder(id);
    }


    @Test
    public void testAddProductToOrderIfNotExist() {
        Long orderId = orderDAO.create(order);
        try {
            orderService.addProductToOrder(orderId, 1L);
            Order refreshOrder = orderDAO.findByIdWithOrderLines(orderId);
            assertNotNull("Order line must be found", refreshOrder.getOrderLines());
            assertEquals("Order lines size must be 1:", 1, refreshOrder.getOrderLines().size());
            assertEquals("Order total must be $559.00:", Double.valueOf(559.00), refreshOrder.getTotal());
        } catch (NotEnoughProductsException e) {
            fail("NotEnoughProductsException" + e.getMessage());
        } finally {
            productDAO.increaseAvailableCount(1L, 1);
            orderDAO.deleteOrder(orderId);
        }
    }

    @Test
    public void testAddProductToOrderIfExist() {
        Long orderId = orderDAO.create(order);
        try {
            orderService.addProductToOrder(orderId, 1L);
            orderService.addProductToOrder(orderId, 1L);
            Order refreshOrder = orderDAO.findByIdWithOrderLines(orderId);
            assertNotNull("Order line must be found", refreshOrder.getOrderLines());
            assertEquals("Order lines size must be 1:", 1, refreshOrder.getOrderLines().size());
            assertEquals("Order line quantity must be 2", Integer.valueOf(2), refreshOrder.getOrderLines().get(0).getQuantity());
            assertEquals("Order total must be $1118.00:", Double.valueOf(559.00 * 2), refreshOrder.getTotal());
        } catch (NotEnoughProductsException e) {
            fail("NotEnoughProductsException" + e.getMessage());
        } finally {
            productDAO.increaseAvailableCount(1L, 2);
            orderDAO.deleteOrder(orderId);
        }
    }

    @Test(expected = NotEnoughProductsException.class)
    public void testAddProductToOrderIfNotEnoughProducts() throws NotEnoughProductsException {
        long orderId = 0L;
        try {
            orderId = orderDAO.create(order);
            orderService.addProductToOrder(orderId, 4L);
        } finally {
            orderDAO.deleteOrder(orderId);
        }
    }

    @Test
    public void testAddOrder() throws Exception {
        Long orderId = orderDAO.create(order);
        orderDAO.findByIdWithOrderLines(orderId);
        orderDAO.deleteOrder(orderId);
    }

    @Test
    public void testRemoveProductFromOrder() {
        Long orderId = orderDAO.create(order);
        try {
            orderService.addProductToOrder(orderId, 2L);
            Order refreshOrder = orderDAO.findByIdWithOrderLines(orderId);
            assertNotNull("Order line must be found", refreshOrder.getOrderLines());
            assertEquals("Order lines size must be 1:", 1, refreshOrder.getOrderLines().size());
            assertEquals("Order total must be $559.00:", Double.valueOf(670.0), refreshOrder.getTotal());
            orderService.removeProductFromOrder(orderId, 2L);
            Product refreshProduct = productDAO.findById(2L);
            assertEquals("Product count must be 4", Long.valueOf(5), refreshProduct.getCountAvailable());
            Order refreshOrder2 = orderDAO.findByIdWithOrderLines(orderId);
            assertNotNull("Order line must be found", refreshOrder2.getOrderLines());
            assertEquals("Order lines size must be 1:", 0, refreshOrder2.getOrderLines().size());
            assertEquals("Order total must be %0.00:", Double.valueOf(0), refreshOrder2.getTotal());
        } catch (NotEnoughProductsException e) {
            fail("NotEnoughProductsException" + e.getMessage());
        } finally {
            orderDAO.deleteOrder(orderId);
        }
    }

    @Test
    public void testRejectOrder() {
        try {
            Long orderId = orderDAO.create(order);
            Long countInProduct1Dao = productDAO.findById(1L).getCountAvailable();
            Long countInProduct2Dao = productDAO.findById(2L).getCountAvailable();
            orderService.addProductToOrder(orderId, 1L);
            orderService.addProductToOrder(orderId, 2L);
            Order refreshOrder = orderDAO.findByIdWithOrderLines(orderId);
            assertNotNull("Order line must be found", refreshOrder.getOrderLines());
            assertEquals("Order lines size must be 2:", 2, refreshOrder.getOrderLines().size());
            assertEquals("Order total must be $1229.00:", Double.valueOf(1229.00), refreshOrder.getTotal());

            orderService.rejectOrder(orderId);

            Order refreshOrder2 = orderDAO.findByIdWithOrderLines(orderId);
            assertNull("Order is deleted", refreshOrder2);
            assertEquals("Quantity of products in 1st line in order deleted and goes back to available products:"
                    , countInProduct1Dao, productDAO.findById(1L).getCountAvailable());
            assertEquals("Quantity of products in 2nd line in order deleted and goes back to available products:"
                    , countInProduct2Dao, productDAO.findById(2L).getCountAvailable());

            orderService.rejectOrder(323L);
        } catch (NotEnoughProductsException e) {
            fail("NotEnoughProductsException" + e.getMessage());
        }
    }

    @Test
    public void testReceiveOrder() throws Exception {
        Long id = orderDAO.create(order);
        orderService.reviseOrder(id);
        Order updateOrder = orderDAO.findById(id);
        assertEquals(OrderStatus.PACKAGING, updateOrder.getOrderStatus());
        orderDAO.deleteOrder(id);
    }

    @Test
    public void testDeliveryOrder() throws Exception {
        Long id = orderDAO.create(order);
        orderService.packageOrder(id);
        Order updateOrder = orderDAO.findById(id);
        assertEquals(OrderStatus.DELIVERING, updateOrder.getOrderStatus());
        orderDAO.deleteOrder(id);
    }
}
