package com.exadel.i.webshop.test.util;

import com.exadel.i.webshop.util.PagerUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static junit.framework.Assert.assertEquals;

/**
 * @author Andrey Frunt
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app-config.xml","classpath:integration-config.xml"})
public class PagerUtilTest {

    @Test
    public void testOffset() {
        assertEquals(8, PagerUtil.calculateOffset(17, 2));
        assertEquals(0, PagerUtil.calculateOffset(0, 0));
        assertEquals(0, PagerUtil.calculateOffset(1, 0));
        assertEquals(0, PagerUtil.calculateOffset(0, 1));
        assertEquals(3, PagerUtil.calculateOffset(1, 1, 1, PagerUtil.DEFAULT_PER_PAGE));
    }

    @Test
    public void testPageCount() {
        assertEquals(2, PagerUtil.calculatePageCount(17, 15));
        assertEquals(5, PagerUtil.calculatePageCount(17, -5));
        assertEquals(4, PagerUtil.calculatePageCount(15, 0, PagerUtil.DEFAULT_PER_PAGE));
    }
}
