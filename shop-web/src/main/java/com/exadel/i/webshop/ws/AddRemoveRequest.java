package com.exadel.i.webshop.ws;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author Andrey Frunt
 */
@XmlRootElement
public class AddRemoveRequest implements Serializable {
    private long orderId;
    private long productId;

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }
}
