package com.exadel.i.webshop.dao;

import com.exadel.i.webshop.domain.ShippingAddress;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Andrey Frunt
 */
@Repository
public class ShippingAddressDAO {
    private static final ShippingAddressMapper mapper = new ShippingAddressMapper();
    @Resource
    private JdbcTemplate template;

    public List<ShippingAddress> findByUserId(Long userId) {
        return template.query("SELECT * FROM ShippingAddresses WHERE userId=?", new Object[]{userId}, mapper);
    }

    public int create(ShippingAddress shippingAddress) {
        return template.update("INSERT into ShippingAddresses( userId, name, state, city, address,zip) values(?,?,?,?,?,?)",
                new Object[]{
                        shippingAddress.getUserId(),
                        shippingAddress.getName(),
                        shippingAddress.getState(),
                        shippingAddress.getCity(),
                        shippingAddress.getAddress(),
                        shippingAddress.getZip()});
    }

    public int delete(Long id) {
        return template.update("DELETE FROM ShippingAddresses WHERE id=?", new Object[]{id});
    }

    public ShippingAddress findById(Long id) {
        return template.queryForObject("SELECT * FROM ShippingAddresses WHERE id=?", new Object[]{id}, mapper);
    }

    private static final class ShippingAddressMapper implements RowMapper<ShippingAddress> {
        @Override
        public ShippingAddress mapRow(ResultSet rs, int i) throws SQLException {
            ShippingAddress sa = new ShippingAddress();
            sa.setId(rs.getLong("id"));
            sa.setUserId(rs.getLong("userId"));
            sa.setName(rs.getString("name"));
            sa.setState(rs.getString("state"));
            sa.setCity(rs.getString("city"));
            sa.setAddress(rs.getString("address"));
            sa.setZip(rs.getString("zip"));
            return sa;
        }
    }
}
