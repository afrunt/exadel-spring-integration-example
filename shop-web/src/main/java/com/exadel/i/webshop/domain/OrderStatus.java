package com.exadel.i.webshop.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Andrey Frunt
 */
public enum OrderStatus {
    NEW(1), REVISING(10), REVISED(20), PACKAGING(30), DELIVERING(60);

    private int value;
    private static final Map<Integer, OrderStatus> enumMap = new HashMap<Integer, OrderStatus>();

    static {
        for (OrderStatus v : values()) {
            enumMap.put(v.getValue(), v);
        }
    }

    OrderStatus(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;

    }

    public static OrderStatus fromInt(int value) {
        return enumMap.get(value);
    }
}
