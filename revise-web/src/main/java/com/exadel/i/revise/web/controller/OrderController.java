package com.exadel.i.revise.web.controller;

import com.exadel.i.revise.domain.Order;
import com.exadel.i.revise.domain.Product;
import com.exadel.i.revise.service.OrderService;
import com.exadel.i.revise.utils.Message;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static com.exadel.i.revise.utils.Message.Type.WARN;

/**
 * @author Andrey Frunt
 */
@Controller
@RequestMapping("/orders")
public class OrderController {
    @Resource
    private OrderService orderService;
    @Resource
    private MessageSource messageSource;

    @RequestMapping(value = {"/", ""}, method = RequestMethod.GET)
    public String list(Model uiModel) {
        List<Order> allOrders = orderService.findAll();
        if (!allOrders.isEmpty()) {
            return show(allOrders.get(0).getId(), uiModel);
        } else {
            uiModel.addAttribute("orders", allOrders);
            return "list-all";
        }
    }

    @RequestMapping(value = {"/{id}", "/{id}/"}, method = RequestMethod.GET)
    public String show(@PathVariable Long id, Model uiModel) {
        uiModel.addAttribute("orders", orderService.findAll());
        uiModel.addAttribute("currentOrder", orderService.findById(id));
        return "show-order";
    }

    @RequestMapping(value = {"/revise", "/revise/"}, method = RequestMethod.POST)
    public String revise(@RequestParam(value = "reviseOrderId") Long id, Model uiModel) {
        orderService.reviseOrder(id);
        uiModel.addAttribute("orders", orderService.findAll());
        return "redirect:/orders/";
    }

    @RequestMapping(value = {"/reject", "/reject/"}, method = RequestMethod.POST)
    public String reject(@RequestParam(value = "rejectOrderId") Long id, Model uiModel) {
        orderService.rejectOrder(id);
        uiModel.addAttribute("orders", orderService.findAll());
        return "redirect:/orders/";
    }

    @RequestMapping(value = {"/remove", "/remove/"}, method = RequestMethod.POST)
    public String remove(@RequestParam(value = "removeOrderId") Long orderId, @RequestParam(value = "removeProductId") Long productId, Model uiModel,
                         RedirectAttributes redirectAttributes) {
        try {
            orderService.removeProductFromOrder(orderId, productId);
        } catch (Exception e) {
            Message message = new Message(WARN, e.getMessage());
            redirectAttributes.addFlashAttribute("messages", Arrays.asList(message));
        }
        uiModel.addAttribute("orders", orderService.findAll());
        Order order = orderService.findById(orderId);
        if (order != null) {
            return "redirect:/orders/" + orderId;
        } else {
            return "redirect:/orders/";
        }
    }

    @ResponseBody
    @RequestMapping(value = {"/rest/find-by-keyword", "/rest/find-by-keyword/"}, method = RequestMethod.POST, produces = "application/json")
    public List<Product> findProductsByKeyword(@RequestParam(value = "keyword") String keyword) {
        return orderService.findProductsByKeyword(keyword);
    }

    @RequestMapping(value = {"/rest/addProduct", "/rest/addProduct/"}, method = RequestMethod.POST)
    public String addProduct(@RequestParam(value = "orderId") Long orderId, @RequestParam(value = "productId") Long productId,
                             RedirectAttributes redirectAttributes) {
        try {
            orderService.addProductToOrder(orderId, productId);
        } catch (Exception e) {
            Message message = new Message(WARN, e.getMessage());
            redirectAttributes.addFlashAttribute("messages", Arrays.asList(message));
        }
        return "redirect:/orders/" + orderId;
    }

    @ResponseBody
    @RequestMapping(value = "/rest/find-all", method = RequestMethod.GET, produces = "application/json")
    public List<Order> findAll() {
        return orderService.findAll();
    }
}
