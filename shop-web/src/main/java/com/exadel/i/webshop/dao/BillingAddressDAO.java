package com.exadel.i.webshop.dao;

import com.exadel.i.webshop.domain.BillingAddress;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Andrey Frunt
 */
@Repository
public class BillingAddressDAO {
    private static final BillingAddressMapper mapper = new BillingAddressMapper();
    @Resource
    private JdbcTemplate template;

    public List<BillingAddress> findByUserId(Long userId) {
        return template.query("SELECT * FROM BillingAddresses WHERE userId=?", new Object[]{userId}, mapper);
    }

    public int create(BillingAddress address) {
        return template.update("INSERT INTO BillingAddresses (userId, name, state, city, address) VALUES (?,?,?,?,?)",
                new Object[]{address.getUserId(), address.getName(), address.getState(), address.getCity(), address.getAddress()});
    }

    public int delete(Long id) {
        return template.update("DELETE FROM BillingAddresses WHERE id=?", new Object[]{id});
    }

    public BillingAddress findById(Long id) {
        return template.queryForObject("SELECT * FROM BillingAddresses WHERE id=?", new Object[]{id}, mapper);
    }

    private static final class BillingAddressMapper implements RowMapper<BillingAddress> {
        @Override
        public BillingAddress mapRow(ResultSet rs, int i) throws SQLException {
            BillingAddress sa = new BillingAddress();
            sa.setId(rs.getLong("id"));
            sa.setUserId(rs.getLong("userId"));
            sa.setName(rs.getString("name"));
            sa.setState(rs.getString("state"));
            sa.setCity(rs.getString("city"));
            sa.setAddress(rs.getString("address"));
            return sa;
        }
    }
}
