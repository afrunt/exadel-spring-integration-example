package com.exadel.i.webshop.web.controllers;

import com.exadel.i.webshop.dao.CategoryDAO;
import com.exadel.i.webshop.service.ShoppingCartService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * @author Andrey Frunt
 */
@Controller
@RequestMapping("/")
public class LoginController {
    @Resource
    private ShoppingCartService shoppingCartService;
    @Resource
    private CategoryDAO categoryDAO;

    @RequestMapping("login")
    public String showLoginPage(Model uiModel, HttpSession session) {
        uiModel.addAttribute("categories", categoryDAO.findAll());
        uiModel.addAttribute("cart", shoppingCartService.getCart(session));
        uiModel.addAttribute("title", "Login page");
        return "login-page";
    }
}
