$(function () {
    var getProductListByKeyword = function () {
        $.ajax({
            url:url_find_by_keyword,
            type:'POST',
            data:"keyword=" + $('[name=keyword]').val(),
            dataType:"json",
            success:function (data) {
                var el = $("#productsList");
                el.empty();
                if (data.length > 0) {
                    var productList = "";
                    for (key in data) {
                        var product = data[key];
                        var markup;
                        if (product.countAvailable > 0) {
                            markup = '<tr>' +
                                '<td>' + product.name + '</td>' +
                                '<td><img src=' + product.imageUrl + ' style="max-height: 35px"/></td>' +
                                '<td>' + product.countAvailable + '</td>' +
                                '<td>' + product.price + '</td>' +
                                '<td>' +
                                '<form method="post" action="' + url_add_product_url + '">' +
                                '<input type="hidden" name="orderId" value="' + currentOrderId + '"/>' +
                                '<input type="hidden" name="productId" value="' + product.id + '"/>' +
                                '<button type="submit" class="btn">Add</button>' +
                                '</form></td>' +
                                '</tr>';
                        } else {
                            markup = '<tr>' +
                                '<td>' + product.name + '</td>' +
                                '<td><img src=' + product.imageUrl + ' style="max-height: 35px"/></td>' +
                                '<td>' + product.countAvailable + '</td>' +
                                '<td>' + product.price + '</td>' +
                                '<td>' +
                                '<button type="button" class="btn" disabled="disabled">Add</button>' +
                                '</td>' +
                                '</tr>';
                        }

                        productList = productList + markup;
                    }
                    el.append(productList);
                    el.wrapInner('<table class="table" />');
                } else {
                    el.append('<h5>Products not found</h5>');
                }
            },
            error:function (data) {
                console.log(data);
            }
        });
    };
    $('form.products').on("submit", function (event) {
        event.preventDefault();
        var keyword = $('[name=keyword]').val();
        if (keyword.length > 0)
            getProductListByKeyword();
        else {
            $("#productsList").empty();
            $("#productsList").append('<h5>Enter keyword for search</h5>');
        }
    });
});