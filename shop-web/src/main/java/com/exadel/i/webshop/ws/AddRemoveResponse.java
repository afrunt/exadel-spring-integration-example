package com.exadel.i.webshop.ws;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author Andrey Frunt
 */
@XmlRootElement
public class AddRemoveResponse implements Serializable {
    private boolean success;
    private String errorMessage;
    private ResponseOrder responseOrder;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public ResponseOrder getResponseOrder() {
        return responseOrder;
    }

    public void setResponseOrder(ResponseOrder responseOrder) {
        this.responseOrder = responseOrder;
    }
}
