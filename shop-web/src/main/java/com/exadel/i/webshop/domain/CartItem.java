package com.exadel.i.webshop.domain;

/**
 * @author Andrey Frunt
 */
public class CartItem {
    private long productId;
    private int quantity;
    private double price;

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getSum() {
        return quantity * price;
    }
}
