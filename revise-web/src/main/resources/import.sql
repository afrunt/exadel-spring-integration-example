INSERT INTO Roles (id, name) VALUES (1, 'USER');
INSERT INTO Roles (id, name) VALUES (2, 'ADMIN');

INSERT INTO Users (id, firstName, lastName, email, passwordHash)
  VALUES (1, 'John', 'Doe', 'john.doe@gmail.com', md5('test', 'UTF-8'));

INSERT INTO UserRoles (userId, roleId) VALUES (1, 2);
