package com.exadel.i.revise.test.dao;

import com.exadel.i.revise.dao.OrderDAO;
import com.exadel.i.revise.domain.Order;
import com.exadel.i.revise.domain.OrderLine;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Ksenia Orlenko
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app-config.xml", "classpath:integration-config.xml"})
public class OrderDAOTest {
    @Resource
    private OrderDAO orderDAO;
    private final Order order = new Order();
    private final OrderLine orderLine = new OrderLine();

    @Before
    public void init() {
        order.setId(1L);
        order.setBillingAddress("Billing Address");
        order.setShippingAddress("Shipping address");
        order.setTotal(500.00);
        order.setPhoneNumber("01234567899");
        orderLine.setId(1L);
        orderLine.setProductId(1L);
        orderLine.setImageUrl("image url");
        orderLine.setName("Order line name");
        orderLine.setPrice(500.00);
        orderLine.setQuantity(1);
        orderLine.setOrder(order);
    }

    @Test
    public void testFindById() throws Exception {
        orderDAO.saveOrder(order);
        Order orderFounded = orderDAO.findById(order.getId());
        Assert.assertNotNull("Order must be founded", orderFounded);
        Assert.assertEquals("Order id must be 1", order.getId(), orderFounded.getId());
        orderDAO.delete(order);
    }

    @Test
    public void testFindAll() throws Exception {
        orderDAO.saveOrder(order);
        List<Order> all = orderDAO.findAll();
        Assert.assertNotNull("Order must be founded", all);
        Assert.assertEquals("Order list size must be 1", 1, all.size());
        orderDAO.delete(order);
    }

    @Test
    public void testSaveOrderLine() throws Exception {
        orderDAO.saveOrder(order);
        orderDAO.saveOrderLine(orderLine);
        Order orderFounded = orderDAO.findById(order.getId());
        Assert.assertNotNull("Order must be founded", orderFounded);
        Assert.assertEquals("Order id must be 1", order.getId(), orderFounded.getId());
        Assert.assertNotNull("OrderLine must be not null", orderFounded.getOrderLines());
        Assert.assertEquals("OrderLine list size must be 1", 1, orderFounded.getOrderLines().size());
        orderDAO.delete(order);
    }
}
