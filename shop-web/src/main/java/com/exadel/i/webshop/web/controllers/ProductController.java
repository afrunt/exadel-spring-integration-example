package com.exadel.i.webshop.web.controllers;

import com.exadel.i.webshop.dao.CategoryDAO;
import com.exadel.i.webshop.dao.ProductDAO;
import com.exadel.i.webshop.domain.Product;
import com.exadel.i.webshop.service.ShoppingCartService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * @author Andrey Frunt
 */
@Controller
@RequestMapping("/product")
public class ProductController {
    @Resource
    private ProductDAO productDAO;
    @Resource
    private CategoryDAO categoryDAO;
    @Resource
    private ShoppingCartService shoppingCartService;

    @RequestMapping(value = {"/{id}", "/{id}/"}, method = GET)
    public String show(@PathVariable Long id, Model uiModel, HttpSession session) {
        Product product = productDAO.findById(id);
        uiModel.addAttribute("title", product.getName());
        uiModel.addAttribute("product", product);
        uiModel.addAttribute("categories", categoryDAO.findAll());
        uiModel.addAttribute("currentCategoryId", product.getCategoryId());
        uiModel.addAttribute("cart", shoppingCartService.getCart(session));
        return "product-show";
    }
}
