package com.exadel.i.webshop.test.dao;

import com.exadel.i.webshop.dao.BillingAddressDAO;
import com.exadel.i.webshop.domain.BillingAddress;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

import static junit.framework.Assert.*;

/**
 * @author Andrey Frunt
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app-config.xml","classpath:integration-config.xml"})
public class BillingAddressDAOTest {
    @Resource
    private BillingAddressDAO billingAddressDao;
    private final BillingAddress billingAddress = new BillingAddress();

    @Before
    public void init() {
        billingAddress.setUserId(2L);
        billingAddress.setName("Alex");
        billingAddress.setState("CA");
        billingAddress.setCity("Walnut Creek");
        billingAddress.setAddress("1340 Treat Blvd.");
    }
    @Test
    public void testFindById() throws Exception {
        BillingAddress address = billingAddressDao.findById(1L);
        assertNotNull(address);
    }

    @Test
    public void testFindByUserId() {
        List<BillingAddress> billingAddress = billingAddressDao.findByUserId(1L);
        assertEquals("John Doe",billingAddress.get(0).getName());
        assertEquals("CA",billingAddress.get(0).getState());
        assertEquals("Walnut Creek", billingAddress.get(0).getCity());
        assertEquals("1340 Treat Blvd.",billingAddress.get(0).getAddress());

    }
    @Test
    public void testSave() throws Exception {
        billingAddressDao.create(billingAddress);
        List<BillingAddress> billingAddresses = billingAddressDao.findByUserId(2L);
        assertEquals(1, billingAddresses.size());
        assertEquals("Alex", billingAddresses.get(0).getName());
        assertEquals( "CA" , billingAddresses.get(0).getState());
        assertEquals("Walnut Creek", billingAddresses.get(0).getCity());
        assertEquals("1340 Treat Blvd." , billingAddresses.get(0).getAddress());
        billingAddressDao.delete(billingAddresses.get(0).getId());
        List<BillingAddress> billingAddressesRefresh = billingAddressDao.findByUserId(2L);
        assertTrue(billingAddressesRefresh.isEmpty());
    }
}
