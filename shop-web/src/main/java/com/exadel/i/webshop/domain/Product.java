package com.exadel.i.webshop.domain;

import java.io.Serializable;

/**
 * @author Andrey Frunt
 */
public class Product implements Serializable {
    private Long id;
    private String name;
    private String description;
    private Long countAvailable;
    private Double price;
    private Long categoryId;
    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCountAvailable() {
        return countAvailable;
    }

    public void setCountAvailable(Long countAvailable) {
        this.countAvailable = countAvailable;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }
}
