package com.exadel.i.revise.service;

import com.exadel.i.revise.dao.OrderDAO;
import com.exadel.i.revise.domain.*;
import com.exadel.i.revise.ws.*;
import com.exadel.i.revise.ws.ResponseOrder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrey Frunt
 */
@Service
public class OrderService {
    private static final Log log = LogFactory.getLog(OrderService.class);
    @Resource
    private OrderDAO orderDAO;
    @Resource(name = "revisedOrderOutputChannel")
    private MessageChannel reviseChannel;
    @Resource(name = "rejectOrderOutputChannel")
    private MessageChannel rejectChannel;
    @Resource(name = "searchRequests")
    private MessageChannel searchChannel;
    @Resource(name = "addRequests")
    private MessageChannel addChannel;
    @Resource(name = "removeRequests")
    private MessageChannel removeChannel;

    public List<Order> findAll() {
        return orderDAO.findAll();
    }

    public Order findById(Long id) {
        return orderDAO.findById(id);
    }

    /**
     * Send soap request to shop-web and receive response to find product by keywords
     *
     * @param keyword - keywords
     * @return - list of founded product
     */
    public List<Product> findProductsByKeyword(String keyword) {
        log.info("Finding products by keyword: " + keyword);
        List<Product> products = new ArrayList<Product>();
        MessagingTemplate messagingTemplate = new MessagingTemplate();
        SearchRequest request = new SearchRequest();
        request.setSearchTerm(keyword);
        Message<SearchRequest> message = MessageBuilder.withPayload(request).build();
        Message<SearchResponse> response = (Message<SearchResponse>) messagingTemplate.sendAndReceive(searchChannel, message);
        SearchResponse searchResults = response.getPayload();

        List<SearchResponse.SearchResponseItem> items = searchResults.getItems();

        for (SearchResponse.SearchResponseItem responseItem : items) {
            Product product = new Product();
            product.setName(responseItem.getName());
            product.setPrice(responseItem.getPrice());
            product.setImageUrl(responseItem.getImageUrl());
            product.setCountAvailable(responseItem.getCountAvailable());
            product.setId(responseItem.getProductId());
            products.add(product);
        }

        log.info("Products found by keyword: " + keyword + ".Message request sent to web-shop and response received.Found Products=" + searchResults.getItems().size());
        return products;
    }

    /**
     * Send soap request to shop-web and receive response to add product to order
     *
     * @param orderId   - order id
     * @param productId - product id
     * @throws Exception - throw if cant add product
     */
    @Transactional(rollbackFor = Exception.class)
    public void addProductToOrder(Long orderId, Long productId) throws Exception {
        log.info("Adding product to order with id=" + orderId + "and productId=" + productId );
        MessagingTemplate messagingTemplate = new MessagingTemplate();
        AddRemoveRequest request = new AddRemoveRequest();
        request.setOrderId(orderId);
        request.setProductId(productId);
        Message<AddRemoveRequest> message = MessageBuilder.withPayload(request).build();
        Message<AddRemoveResponse> response = (Message<AddRemoveResponse>) messagingTemplate.sendAndReceive(addChannel, message);
        AddRemoveResponse addResult = response.getPayload();

        log.info("Product added.Id=" + orderId + ".ProductId=" + productId + ".Message request sent to web-shop and response received.SUCCESS=" + addResult.isSuccess());

        if (addResult.isSuccess()) {
            ResponseOrder responseOrder = addResult.getResponseOrder();
            createNewOrder(responseOrder);
        } else {
            log.error(addResult.getErrorMessage());
            throw new Exception(addResult.getErrorMessage());
        }
    }

    /**
     * Convert order response and created new order in database
     *
     * @param responseOrder - order response from shop-web
     */
    @Transactional
    private void createNewOrder(ResponseOrder responseOrder) {
        Order oldOrder = orderDAO.findById(responseOrder.getId());
        orderDAO.delete(oldOrder);
        Order order = new Order();
        log.info("Creating new order.Id=" + order.getId());
        order.setId(responseOrder.getId());
        order.setBillingAddress(responseOrder.getBillingAddress());
        order.setShippingAddress(responseOrder.getShippingAddress());
        order.setPhoneNumber(responseOrder.getPhoneNumber());
        order.setTotal(responseOrder.getTotal());
        orderDAO.saveOrder(order);
        for (ResponseOrder.ResponseOrderLine responseOrderLine : responseOrder.getOrderLines()) {
            OrderLine orderLine = new OrderLine();
            orderLine.setId(responseOrderLine.getId());
            orderLine.setProductId(responseOrderLine.getProductId());
            orderLine.setPrice(responseOrderLine.getPrice());
            orderLine.setQuantity(responseOrderLine.getQuantity());
            orderLine.setImageUrl(responseOrderLine.getImageUrl());
            orderLine.setName(responseOrderLine.getName());
            orderLine.setOrder(order);
            orderDAO.saveOrderLine(orderLine);
        }
        log.info("New order created.Id=" + order.getId());
    }

    /**
     * Received new order from shop-web and created order
     *
     * @param message - object that contains order
     */
    @Transactional
    public void receiveNewOrder(OrderMessage message) {
        Order order = new Order();
        log.info("Receiving new order.Id=" + order.getId());
        order.setId(message.getId());
        order.setBillingAddress(message.getBillingAddress());
        order.setShippingAddress(message.getShippingAddress());
        order.setPhoneNumber(message.getPhoneNumber());
        order.setTotal(message.getTotal());
        orderDAO.saveOrder(order);
        List<OrderMessage.MessageOrderLine> messageOrderLines = message.getOrderLines();
        for (OrderMessage.MessageOrderLine messageOrderLine : messageOrderLines) {
            OrderLine orderLine = new OrderLine();
            orderLine.setId(messageOrderLine.getId());
            orderLine.setProductId(messageOrderLine.getProductId());
            orderLine.setImageUrl(messageOrderLine.getImageUrl());
            orderLine.setName(messageOrderLine.getName());
            orderLine.setOrder(order);
            orderLine.setPrice(messageOrderLine.getPrice());
            orderLine.setQuantity(messageOrderLine.getQuantity());
            orderDAO.saveOrderLine(orderLine);
        }
        log.info("New order received.Id=" + order.getId());
    }

    /**
     * Delete order from database and sen revise message to shop-web
     *
     * @param id - order id
     */
    @Transactional
    public void reviseOrder(Long id) {
        Order order = orderDAO.findById(id);
        orderDAO.delete(order);
        reviseChannel.send(MessageBuilder.withPayload(id).build());
        log.info("Order revised.Id=" + order.getId() + ".Message sent to packaging.");
    }

    /**
     * Delete order from database and send rejected message to shop-web
     *
     * @param id - order id
     */
    @Transactional
    public void rejectOrder(Long id) {
        Order order = orderDAO.findById(id);
        orderDAO.delete(order);
        rejectChannel.send(MessageBuilder.withPayload(id).build());
        log.info("Order rejected.Id=" + order.getId() + ".Message sent to web-shop.");
    }

    /**
     * Send soap request to shop-web and receive response to delete product from order
     *
     * @param orderId   - order id
     * @param productId - product id
     * @throws Exception - throws if cant remove product
     */
    @Transactional(rollbackFor = Exception.class)
    public void removeProductFromOrder(Long orderId, Long productId) throws Exception {
        log.info("Removing product from order.Id=" + orderId + ".ProductId=" + productId);
        MessagingTemplate messagingTemplate = new MessagingTemplate();
        AddRemoveRequest removeRequest = new AddRemoveRequest();
        removeRequest.setOrderId(orderId);
        removeRequest.setProductId(productId);
        Message<AddRemoveRequest> message = MessageBuilder.withPayload(removeRequest).build();
        Message<AddRemoveResponse> response = (Message<AddRemoveResponse>) messagingTemplate.sendAndReceive(removeChannel, message);
        AddRemoveResponse removeResult = response.getPayload();

        log.info("Product removed from order.Id=" + orderId + ".ProductId=" + productId + ".Message request sent to web-shop and response received.SUCCESS=" + removeResult.isSuccess());

        if (removeResult.isSuccess()) {
            if (removeResult.getResponseOrder() != null) {
                ResponseOrder responseOrder = removeResult.getResponseOrder();
                createNewOrder(responseOrder);
            } else {
                Order order = orderDAO.findById(orderId);
                orderDAO.delete(order);
            }
        } else {
            log.error(removeResult.getErrorMessage());
            throw new Exception(removeResult.getErrorMessage());
        }
    }
}
