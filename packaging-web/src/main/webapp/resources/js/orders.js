$(function () {
    var refreshOrdersList = function () {
        $.ajax({
            url:urlRestFindAll,
            dataType:"json",
            success:function (data) {
                var ordersList = $("#orders-list");
                ordersList.empty();
                for (var key in data) {
                    if (data.hasOwnProperty(key)) {
                        var order = data[key];
                        var li_class = "";
                        if (currentOrderId && currentOrderId != undefined && currentOrderId == order.id) {
                            li_class = " class='active' ";
                        }
                        ordersList.append($("<li" + li_class + "><a href='" + urlToOrder + order.id + "'>Order " + order.id + "</a></li>"));
                    }
                }
            },
            error: function(data) {
                console.log(data);
            }
        });
    };
    setInterval(refreshOrdersList, 5000);
});
