package com.exadel.i.packaging.service;

import com.exadel.i.packaging.dao.OrderDAO;
import com.exadel.i.packaging.domain.Order;
import com.exadel.i.packaging.domain.OrderLine;
import com.exadel.i.packaging.domain.OrderMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Ksenia Orlenko
 */
@Repository
public class OrderService {

    private static final Log log = LogFactory.getLog(OrderService.class);

    @Resource
    private OrderDAO orderDAO;
    @Resource(name = "packagingOrderOutputChannel")
    private MessageChannel channel;

    public List<Order> findAll() {
        return orderDAO.findAll();
    }

    public Order findById(Long id) {
        return orderDAO.findById(id);
    }

    /**
     * Receives order from a web-shop and save it in the database
     *
     * @param message object receives from channel
     */
    @Transactional
    public void packagingNewOrder(OrderMessage message) {
        log.info("Received message with order id:" + message.getId() + " from web-shop");
        Order order = new Order();
        order.setId(message.getId());
        order.setBillingAddress(message.getBillingAddress());
        order.setShippingAddress(message.getShippingAddress());
        order.setPhoneNumber(message.getPhoneNumber());
        order.setTotal(message.getTotal());
        orderDAO.create(order);
        log.info("Save order with id:" + order.getId());
        List<OrderMessage.MessageOrderLine> messageOrderLines = message.getOrderLines();
        for (OrderMessage.MessageOrderLine messageOrderLine : messageOrderLines) {
            OrderLine orderLine = new OrderLine();
            orderLine.setId(messageOrderLine.getId());
            orderLine.setProductId(messageOrderLine.getProductId());
            orderLine.setImageUrl(messageOrderLine.getImageUrl());
            orderLine.setName(messageOrderLine.getName());
            orderLine.setOrder(order);
            orderLine.setPrice(messageOrderLine.getPrice());
            orderLine.setQuantity(messageOrderLine.getQuantity());
            orderDAO.createOrderLine(orderLine);
            log.info("Save order line with id:" + orderLine.getId());
        }
    }

    /**
     * Send by channel order id to web-shop and delete order form database
     *
     * @param id order id
     */
    @Transactional
    public void packagingOrder(Long id) {
        Order order = orderDAO.findById(id);
        orderDAO.delete(order);
        log.info("Delete order with id:" + order.getId());
        channel.send(MessageBuilder.withPayload(id).build());
        log.info("Confirm packaging order with id:" + order.getId() + " and send message to web-shop");
    }

}
