package com.exadel.i.webshop.web.controllers;

import com.exadel.i.webshop.dao.BillingAddressDAO;
import com.exadel.i.webshop.dao.CategoryDAO;
import com.exadel.i.webshop.dao.ProductDAO;
import com.exadel.i.webshop.dao.ShippingAddressDAO;
import com.exadel.i.webshop.domain.*;
import com.exadel.i.webshop.service.OrderService;
import com.exadel.i.webshop.service.ShoppingCartService;
import com.exadel.i.webshop.service.exception.NotEnoughProductsException;
import com.exadel.i.webshop.util.Message;
import org.springframework.context.MessageSource;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.*;

import static com.exadel.i.webshop.util.Message.Type.WARN;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

/**
 * @author Andrey Frunt
 */
@Controller
@RequestMapping("/shoppingCart")
public class ShoppingCartController {
    @Resource
    private ProductDAO productDAO;
    @Resource
    private ShoppingCartService cartService;
    @Resource
    private CategoryDAO categoryDAO;
    @Resource
    private ShippingAddressDAO shippingAddressDAO;
    @Resource
    private BillingAddressDAO billingAddressDAO;
    @Resource
    private OrderService orderService;
    @Resource
    private MessageSource messageSource;

    @RequestMapping(value = {"/show/", "/show"}, method = RequestMethod.GET)
    public String show(Model uiModel, HttpSession session) {
        uiModel.addAttribute("categories", categoryDAO.findAll());
        ShoppingCart cart = cartService.getCart(session);
        uiModel.addAttribute("cart", cart);
        List<CartItem> items = cart.getItems();
        Map<Long, Product> productMap = createProductMap(items);
        if (!productMap.isEmpty()) {
            uiModel.addAttribute("productMap", productMap);
            uiModel.addAttribute("totalSum", cart.getTotal());
        }
        return "cart-show";
    }

    private Map<Long, Product> createProductMap(List<CartItem> items) {
        List<Long> productIds = new ArrayList<Long>(items.size());

        for (CartItem item : items) {
            productIds.add(item.getProductId());
        }

        if (!productIds.isEmpty()) {
            List<Product> products = productDAO.findByIds(productIds);
            Map<Long, Product> productMap = new HashMap<Long, Product>();

            for (Product product : products) {
                productMap.put(product.getId(), product);
            }
            return productMap;
        } else {
            return Collections.emptyMap();
        }
    }

    @RequestMapping(value = {"/remove/", "/remove"}, method = RequestMethod.POST)
    public String removeFromCart(@RequestParam("productId") Long id, Model uiModel, HttpSession session) {
        ShoppingCart cart = cartService.getCart(session);
        cartService.removeFromCart(cart, id);
        return show(uiModel, session);
    }

    @RequestMapping(value = {"/addToCart/", "/addToCart"}, method = POST)
    public String addToCart(@RequestParam("productId") Long id, RedirectAttributes redirectAttributes, Locale locale, HttpSession session) {
        try {
            cartService.addToCart(cartService.getCart(session), id);
        } catch (NotEnoughProductsException e) {
            Message message = new Message(WARN, messageSource.getMessage("warn.notenoughproductstoadd", new Object[]{e.getCountAvailable(), e.getProductName()}, locale));
            redirectAttributes.addFlashAttribute("messages", Arrays.asList(message));
        }
        return "redirect:/product/" + id;
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = {"/createBillingAddress/", "/createBillingAddress"}, method = RequestMethod.GET)
    public String showFormBillingAddress(BillingAddress billingAddress, Model uiModel) {
        uiModel.addAttribute("categories", categoryDAO.findAll());
        return "createBillingAddress-show";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = {"/createBillingAddress/", "/createBillingAddress"}, method = RequestMethod.POST)
    public String createBillingAddress(BillingAddress billingAddress, Model uiModel, HttpSession session) {
        User currentUser = (User) session.getAttribute("CURRENT_USER");
        billingAddress.setUserId(currentUser.getId());
        billingAddressDAO.create(billingAddress);
        uiModel.addAttribute("categories", categoryDAO.findAll());
        return "redirect:/shoppingCart/confirm/";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = {"/createShippingAddress/", "/createShippingAddress"}, method = RequestMethod.GET)
    public String showFormShippingAddress(ShippingAddress shippingAddress, Model uiModel) {
        uiModel.addAttribute("categories", categoryDAO.findAll());
        return "createShippingAddress-show";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = {"/createShippingAddress/", "/createShippingAddress"}, method = RequestMethod.POST)
    public String createShippingAddress(ShippingAddress shippingAddress, Model uiModel, HttpSession session) {
        User currentUser = (User) session.getAttribute("CURRENT_USER");
        shippingAddress.setUserId(currentUser.getId());
        shippingAddressDAO.create(shippingAddress);
        uiModel.addAttribute("categories", categoryDAO.findAll());
        return "redirect:/shoppingCart/confirm/";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = {"/confirm/", "/confirm"}, method = RequestMethod.GET)
    public String showConfirmPage(Model uiModel, HttpSession session) {
        User currentUser = (User) session.getAttribute("CURRENT_USER");
        uiModel.addAttribute("phoneNumber", currentUser.getPhoneNumber());
        List<BillingAddress> billingAddresses = billingAddressDAO.findByUserId(currentUser.getId());
        uiModel.addAttribute("billingAddresses", billingAddresses);
        List<ShippingAddress> shippingAddresses = shippingAddressDAO.findByUserId(currentUser.getId());
        uiModel.addAttribute("shippingAddresses", shippingAddresses);
        cartService.refresh(session);
        ShoppingCart cart = cartService.getCart(session);
        List<CartItem> cartItems = cart.getItems();
        uiModel.addAttribute("cartItems", cartItems);
        Map<Long, Product> productMap = createProductMap(cartItems);
        if (!productMap.isEmpty()) {
            uiModel.addAttribute("productMap", productMap);
            uiModel.addAttribute("totalSum", cart.getTotal());
        }
        uiModel.addAttribute("categories", categoryDAO.findAll());
        return "confirm-show";
    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = {"/confirm/", "/confirm"}, method = RequestMethod.POST)
    public String createOrder(@RequestParam("shippingAddress") Long shippingAddressId, @RequestParam("billingAddress") Long billingAddressId,
                              @RequestParam("phoneNumber") String phoneNumber, Model uiModel, RedirectAttributes redirectAttributes, Locale locale, HttpSession session) {
        ShoppingCart cart = cartService.getCart(session);
        User currentUser = (User) session.getAttribute("CURRENT_USER");
        try {
            orderService.submitOrder(cart, currentUser.getId(), phoneNumber, billingAddressId, shippingAddressId);
        } catch (NotEnoughProductsException e) {
            Message message = new Message(WARN, messageSource.getMessage("warn.notenoughproducts", new Object[]{e.getCountAvailable(), e.getProductName()}, locale));
            redirectAttributes.addFlashAttribute("messages", Arrays.asList(message));
            return "redirect:/shoppingCart/show";
        }
        cartService.clear(cart);
        uiModel.addAttribute("categories", categoryDAO.findAll());
        return "redirect:/profile";
    }
}
