package com.exadel.i.webshop.test.dao;

import com.exadel.i.webshop.dao.RolesDAO;
import com.exadel.i.webshop.domain.Role;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

import static junit.framework.Assert.*;

/**
 * @author Andrey Frunt
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:app-config.xml","classpath:integration-config.xml"})
public class RolesDAOTest {
    @Resource
    private RolesDAO rolesDAO;

    @Test
    public void testFindAll() {
        List<Role> all = rolesDAO.findAll();
        assertFalse("List must not be empty", all.isEmpty());
        assertEquals("Two roles in DB", 2, all.size());
    }

    @Test
    public void testFindUserRoles() {
        List<Role> userRoles = rolesDAO.findUserRoles(1L);
        assertEquals(Long.valueOf(1L), userRoles.get(0).getId());
        assertFalse("List must not be empty", userRoles.isEmpty());
        assertEquals("Two roles in DB", 1, userRoles.size());
        assertEquals("User have one role 'user'", "USER", userRoles.get(0).getName());
        userRoles = rolesDAO.findUserRoles(0L);
        assertTrue("List must be empty. No user with given ID", userRoles.isEmpty());
    }
}
